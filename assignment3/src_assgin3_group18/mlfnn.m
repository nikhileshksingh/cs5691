

%% Load data {'2_linear', '2_non_linear' }
clc;
clear; 



%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data] = load_data('2_linear')


X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 

X_Test  = test_data(:,1:end-1);    
Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);

%%
noOfNeurons = [5 3]
net = patternnet(noOfNeurons,'traingdx');
rng('default') 
net.divideFcn = '';
net.inputs{1}.processFcns ={};
net.trainParam.show = 1;
net.trainParam.epochs =500;
net.trainParam.lr = 0.08; % learning rate 
net.trainParam.goal = 0.01 %Cross entropy
net.trainParam.mc = 0.9 % Momentum constant
[net,tr,y,e] = train(net,X_Train',full(ind2vec(Y_Train')));
 view(net)

%% To calculate output of hidden layer


b1 = net.b{1}
b2 = net.b{2}
b3 = net.b{3}
IW = net.IW{1,1} 
LW_1 = net.LW{2,1}
LW_2 = net.LW{3,2}

%net.layers{1}.transferFcn % To know the function used for 

o_p_h1 = tansig( IW*X_Train' + b1 * ones(1,size(X_Train,1) ) );
o_p_h2 = tansig(LW_1*o_p_h1 + b2*ones(1,size(o_p_h1,2)))
o_p_final = softmax(LW_2*o_p_h2 + b3*ones(1,size(o_p_h2,2)))

% plotting output of  two nodes first hidden layer (node1 node2)

a =X_Train(:,1)
b =X_Train(:,2)

%Hidden layer 1
c = o_p_h1(1,:)'
d = o_p_h1(5,:)'
%Hidden layer 2
e = o_p_h2(1,:)'
f = o_p_h2(3,:)'

% O/P layer 
g = o_p_final(1,:)'
h = o_p_final(2,:)'

% figure;
% grid on
% scatter3(a,b,c,'*r')

%H1 layer
figure
grid on
subplot(3,2,1);
hold on
scatter3(a(1:250,1),b(1:250,1),c(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),c(251:500,1),'*y')
hold off
title('For Node 1(Hidden Layer 1)')
subplot(3,2,2);
hold on
scatter3(a(1:250,1),b(1:250,1),d(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),d(251:500,1),'*y')
hold off
title('For Node 5(Hidden Layer 1)')

%H2 layer
% figure
% grid on
subplot(3,2,3);
hold on
scatter3(a(1:250,1),b(1:250,1),e(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),e(251:500,1),'*y')
hold off
title('For Node 1(Hidden Layer 2)')
subplot(3,2,4);
hold on
scatter3(a(1:250,1),b(1:250,1),f(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),f(251:500,1),'*y')
hold off
title('For Node 3(Hidden Layer 2)')

% O/P layer
% figure
% grid on
subplot(3,2,5);
hold on
scatter3(a(1:250,1),b(1:250,1),g(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),g(251:500,1),'*y')
hold off
title('For Node 1(Output Layer)')
subplot(3,2,6);
hold on
scatter3(a(1:250,1),b(1:250,1),h(1:250,1),'*b')
scatter3(a(251:500,1),b(251:500,1),h(251:500,1),'*y')
hold off
title('For Node 2(Output Layer)')



%% Plot output 
xrange = -2:0.02:3
yrange = -1.5:0.02:1.5

xrange = -10:0.2:20
yrange = -15:0.15:20

[x , y] = meshgrid(xrange,yrange);
mydata = [x(:) y(:)]
pred = net(mydata')
pred = vec2ind(pred);
mydata = [mydata pred']
%%
    figure;
    hold on
    for j = 1 : size(mydata,1)
        if(mydata(j,end) ==1)
            plot(mydata(j,1), mydata(j,2),'.r')
        else if (mydata(j,end) == 2)
            plot(mydata(j,1), mydata(j,2),'.b')
        else if (mydata(j,end) == 3)
            plot(mydata(j,1), mydata(j,2),'.g')        
        else 
            plot(mydata(j,1), mydata(j,2),'.y')
            end
            end
        end
    end
    
 nClasses = max(train_data(:,3))
   points = size(train_data,1)/nClasses
   
   plot(train_data(1:points,1 ), train_data(1:points,2), 'k+','LineWidth', 2, ...
'MarkerSize', 5)

   plot(train_data(points+1:2*points,1 ), train_data(points+1:2*points,2),  'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 5)


   if (nClasses == 3)
       plot(train_data(2*points+1:end,1), train_data(2*points+1:end,2), '^k','LineWidth', 1, ...
'MarkerSize', 5) 
   end
   
 %plot(train_data(:,1),train_data(:,2),  'kd', 'LineWidth', 1, 'MarkerFaceColor', 'g', 'MarkerSize', 7)
    
    
    
    
    
    
%plot(train_data(:,1), train_data(:,2),'*k')
% plot(train_data(:,1),train_data(:,2),  'kd', 'LineWidth', 1, 'MarkerFaceColor', 'g', 'MarkerSize', 7)
hold off
%% Predict and test accuracy after training 

pred = net(X_Train');
pred = vec2ind(pred);
opt1 = full(ind2vec(pred))

accuracy_train = 1 -(size(find(Y_Train' - pred),2)/size(pred,2))

pred = net(X_Val');
pred = vec2ind(pred);
accuracy_val = 1 -(size(find(Y_Val' - pred),2)/size(pred,2))



pred = net(X_Test');
pred = vec2ind(pred);
opt2 =full(ind2vec(pred))
accuracy_test = 1 -(size(find(Y_Test' - pred),2)/size(pred,2))
figure
plotconfusion((full(ind2vec(Y_Train'))),opt1)
title('Train Data')    
% 
figure
plotconfusion((full(ind2vec(Y_Test'))),opt2)
title('Test Data')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
