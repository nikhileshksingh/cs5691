%% Load data {'2_linear', '2_non_linear' }
clc;
clear; 


%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data] = load_data('2_linear')


X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 


X_Test  = test_data(:,1:end-1);    

Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);

Y_Train = Y_Train-1;
Y_Test = Y_Test-1;
Y_Val = Y_Val-1;


%% Encode

a = [1 1 ]';
b = [0 1 ]';
c = [1 0 ]';
%d = [0 0 ]';

A = train_data(find(train_data(:,end) == 1),1:2)'
B = train_data(find(train_data(:,end) == 2),1:2)'
C = train_data(find(train_data(:,end) == 3),1:2)'


P = [A B C ]
% define targets
T = [repmat(a,1,length(A)) repmat(b,1,length(B)) ...
 repmat(c,1,length(C)) ];
plotpv(P,T);


%% Train

net = perceptron;

E = 1;
net.adaptParam.passes = 1;
linehandle = plotpc(net.IW{1},net.b{1});
n = 0;
while ( sse(E) & n<1200)
 n = n+1;
 [net,Y,E] = adapt(net,P,T);
 linehandle = plotpc(net.IW{1},net.b{1},linehandle);
 drawnow;
end
% show perceptron structure
view(net);

%%
pred_Train = decode(net(X_Train'))
pred_Val = decode(net(X_Val'))
pred_Test = decode(net(X_Test'))



accur_Tr =  mean(double(pred_Train == Y_Train)) * 100
accur_V =  mean(double(pred_Val == Y_Val)) * 100
accur_Te =  mean(double(pred_Test == Y_Test)) * 100



%% Decision Surface


xrange = -10:0.2:20
yrange = -15:0.2:20

[x , y] = meshgrid(xrange,yrange);
mydata = [x(:) y(:)]

decision_surface(train_data, mydata, net)




%%

function decision_surface(train_data, mydata, net)
   
     prediction = decode(net(mydata'))
     mydata = [mydata prediction]
   
    figure;
    hold on
    for j = 1 : size(mydata,1)
        if(mydata(j,end) == 0)
            plot(mydata(j,1), mydata(j,2),'.r')
        else if (mydata(j,end) == 1)
            plot(mydata(j,1), mydata(j,2),'.b')
        else if (mydata(j,end) == 2)
            plot(mydata(j,1), mydata(j,2),'.m')        
        else 
            plot(mydata(j,1), mydata(j,2),'.g')
            end
            end
        end
    end
    nClasses = max(train_data(:,3))
    points = size(train_data,1)/nClasses
    
    
    plot(train_data(1:points,1 ), train_data(1:points,2), 'k+','LineWidth', 2, ...
'MarkerSize', 5)
    plot(train_data(points+1:2*points,1 ), train_data(points+1:2*points,2),'ko', 'MarkerFaceColor', 'y', ...
'MarkerSize', 5)


   
        plot(train_data(2*points+1:end,1), train_data(2*points+1:end,2), '^k' , 'LineWidth', 1, ...
'MarkerSize', 5)
   

    hold off

end


%% Decode the output

function pred = decode(y)
tmp = []
for i = 1:size(y,2)
   if (y(1,i) == 0 & y(2,i)== 1)
       tmp(i) = 1
   else if (y(1,i) == 1 & y(2,i)== 1)
           tmp(i) = 0
       else if (y(1,i) == 1 & y(2,i)== 0)
               tmp(i) = 2
           end
       end
   end
    
end
pred = tmp'
end



