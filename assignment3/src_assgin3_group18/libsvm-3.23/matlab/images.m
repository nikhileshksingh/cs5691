
%% Loading data

highway = thedata('highway')
street = thedata('street')
tb = thedata('tb')



%%
data = [highway; street; tb]
dat = data(:,1:end-1)
dat = normalize(dat,2,'range',[-1 1])
data(:,1:end-1) = dat
[train_ind val_ind test_ind] = dividerand(size(data,1), 0.7, 0.2,0.1)

train_data = data(train_ind,:)
val_data = data(val_ind,:)
test_data = data(test_ind,:)

X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 

X_Test  = test_data(:,1:end-1);    
Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);


%%
training_label_vector = X_Train
training_instance_matrix = Y_Train

test_label = X_Val
test_data = Y_Val


%% Polynomial Kernel

model_poly = svmtrain(training_label_vector, training_instance_matrix , '-t 1 -c 10 -r 5 -d  5')
[predict_label_P, accuracy_P, dec_values_P] = svmpredict(test_label, test_data, model_poly)

% %% Gaussian Kernel
% 
% model_gaussian = svmtrain(training_label_vector, training_instance_matrix , '-t 2 -c 1 -g 0.05')
% [predict_label_G, accuracy_G, dec_values_G] = svmpredict(test_label, test_data, model_gaussian);

%%  
function data = thedata(class)

relativePath = '/../../../data_assign3_group18/dataset3/features/';
relativePath = strcat(relativePath,class,'/')
structOfFiles = dir(strcat(pwd,relativePath))

filePath=[]
data = []
filePath = string(filePath)
for i = 3:size(structOfFiles,1)
    filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)
    data = [data; load( filePath(i))]
end

label = 0
switch(class)
    case 'highway'
        label = 1
    case 'street'
        label = 2
    case 'tb'
        label = 3
end
data(:,end+1) = ones(size(data,1),1)*label 
end