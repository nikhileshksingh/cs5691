function [train_data, test_data, val_data] = load_data(i)
        
        
  switch i
    case '2_linear'
        relativePath = '/../../../data_assign3_group18/dataset2/linearly_separable/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:11
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];
        class3_test = [load(filePath(9)) ones(size(load(filePath(9)),1),1).*3];
        class3_train = [load(filePath(10)) ones(size(load(filePath(10)),1),1).*3];
        class3_val = [load(filePath(11)) ones(size(load(filePath(11)),1),1).*3];

        train_data = [class1_train ; class2_train ; class3_train]
        test_data = [class1_test ; class2_test ; class3_test]
        val_data = [class1_val ; class2_val ; class3_val]
    
    case '2_non_linear'
       
        relativePath = '/../../../data_assign3_group18/dataset2/nonlinearly_separable/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:8
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];

        train_data = [class1_train ; class2_train ]
        test_data = [class1_test ; class2_test ]
        val_data = [class1_val ; class2_val ]
    
        case '2_overlapping'
        relativePath = '/../../../data_assign3_group18/dataset2/overlapping/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:14
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];
        class3_test = [load(filePath(9)) ones(size(load(filePath(6)),1),1).*3];
        class3_train = [load(filePath(10)) ones(size(load(filePath(7)),1),1).*3];
        class3_val = [load(filePath(11)) ones(size(load(filePath(8)),1),1).*3];
        class4_test = [load(filePath(12)) ones(size(load(filePath(6)),1),1).*4];
        class4_train = [load(filePath(13)) ones(size(load(filePath(7)),1),1).*4];
        class4_val = [load(filePath(14)) ones(size(load(filePath(8)),1),1).*4];

        train_data = [class1_train ; class2_train ; class3_train; class4_train ]
        test_data = [class1_test ; class2_test ; class3_test; class4_test]
        val_data = [class1_val ; class2_val ; class3_val; class4_val]

        
    otherwise
        disp("No such dataset")




end
