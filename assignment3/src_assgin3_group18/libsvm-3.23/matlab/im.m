
%% Data set 3 
clc 
clear

% [noOfImage1, fetVectSize1 , highway] = thedata('highway')
% [noOfImage2, fetVectSize2 ,street] = thedata('street')
% [noOfImage3, fetVectSize3 ,tb] = thedata('tb')

noOfImage1 = 260
noOfImage2 = 292
noOfImage3 = 356
load('image_data.mat')
%%
%data = [highway; street; tb]

%data(:,1:end-1) = normalize(data(:,1:end-1),2,'range',[-1 1])
%%
 dat = data(:,1:end-1)
 % De-mean
dat = bsxfun(@minus,dat,mean(dat));
 %%

[coeff,score,latent] = pca(dat)
data = [score(:,1:10) data(:,end)]
%%

[train_ind val_ind test_ind] = dividerand(noOfImage1+noOfImage2+noOfImage3, 0.7, 0.2,0.1)
train_d_ind = [];
for i = train_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
    train_d_ind =[train_d_ind [startindx:endindx] ];
end
train_data = data(train_d_ind,:) 

val_d1_ind =[];
val_d2_ind =[];
val_d3_ind =[];

for i = val_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
   if (data(startindx,end)==1)
        val_d1_ind =[val_d1_ind [startindx:endindx] ];
    elseif (data(startindx,end)==2)
        val_d2_ind =[val_d2_ind [startindx:endindx] ];
    else 
       val_d3_ind =[val_d3_ind [startindx:endindx] ];
    end  
end
val_data1 = data(val_d1_ind,:)
val_data2 = data(val_d2_ind,:)
val_data3 = data(val_d3_ind,:)


test_d1_ind =[];
test_d2_ind =[];
test_d3_ind =[];

for i = test_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
    if (data(startindx,end)==1)
        test_d1_ind =[test_d1_ind [startindx:endindx] ];
    elseif (data(startindx,end)==2)
        test_d2_ind =[test_d2_ind [startindx:endindx] ];
    else 
       test_d3_ind =[test_d3_ind [startindx:endindx] ];
    end  
end
test_data1 = data(test_d1_ind,:)
test_data2 = data(test_d2_ind,:)
test_data3 = data(test_d3_ind,:)


%%
val_data = [val_data1; val_data2; val_data3]
test_data = [test_data1;test_data2;test_data3]

X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 

X_Val = val_data(:,1:end-1);
Y_Val = val_data(:, end)

X_Test = test_data(:,1:end-1);
Y_Test = test_data(:, end)


%% Train

training_label_vector = Y_Train
training_instance_matrix = X_Train


test_label = Y_Train
test_data = X_Train

%% Polynomial and Gaussian Kernel

%model_poly = svmtrain(training_label_vector, training_instance_matrix , '-t 1 -c 0.1 -d 4 -h 0')

%model_gaussian = svmtrain(training_label_vector, training_instance_matrix , '-t 2 -c 1 -g 0.5  -h 0')

model_poly = svmtrain(training_label_vector, training_instance_matrix , '-t 1 -c 100 -d 4 -h 0')

%%
[predicted, ~, ~] = svmpredict(test_label, test_data, model_poly)



%% Predict and test after training 
% training accuracy

imageCount = size(X_Train,1)/36
% predicted = vec2ind(y);
count =0;
for i = 1: imageCount
    actualImage = Y_Train(i*36);
    vectPred = predicted(36*(i-1)+1:36*i);
    ratio = [size(find(vectPred == 1),1)/36 ; size(find(vectPred == 2),1)/36 ; size(find(vectPred == 3),1)/36];
   [~ ,predImage] = max(ratio)
   if (actualImage == predImage )
       count = count + 1;
   end
end
train_accuracy  = count/imageCount


%% validation accuracy

imageCount = size(X_Val,1)/36
% predicted = vec2ind(y);
count =0;
for i = 1: imageCount
    actualImage = Y_Val(i*36);
    vectPred = predicted(36*(i-1)+1:36*i);
    ratio = [size(find(vectPred == 1),1)/36 ; size(find(vectPred == 2),1)/36 ; size(find(vectPred == 3),1)/36];
   [~ ,predImage] = max(ratio)
   if (actualImage == predImage )
       count = count + 1;
   end
end
val_accuracy  = count/imageCount

%% Test accuracy

imageCount = size(X_Test,1)/36
% predicted = vec2ind(y);
count =0;
for i = 1: imageCount
    actualImage = Y_Test(i*36);
    vectPred = predicted(36*(i-1)+1:36*i);
    ratio = [size(find(vectPred == 1),1)/36 ; size(find(vectPred == 2),1)/36 ; size(find(vectPred == 3),1)/36];
   [~ ,predImage] = max(ratio)
   if (actualImage == predImage )
       count = count + 1;
   end
end
test_accuracy  = count/imageCount










% 
% %%
% % Validation Accuracy
% totalCountVal = 0;
% count =0;
% % For first Image
% imageCount = size(val_data1,1)/36
% totalCountVal = imageCount + totalCountVal;
% pred = net(val_data1(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = val_data1(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% % For second Image
% imageCount = size(val_data2,1)/36
% totalCountVal = imageCount + totalCountVal;
% pred = net(val_data2(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = val_data2(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% % For third Image
% imageCount = size(val_data3,1)/36
% totalCountVal = imageCount + totalCountVal;
% pred = net(val_data3(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = val_data3(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% 
% accuracy_val = count / totalCountVal
% 
% %%
% % Test Accuracy
% totalCountTest = 0;
% count =0;
% % For first Image
% imageCount = size(test_data1,1)/36
% totalCountTest = imageCount + totalCountTest;
% pred = net(test_data1(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = test_data1(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% % For second Image
% imageCount = size(test_data2,1)/36
% totalCountTest = imageCount + totalCountTest;
% pred = net(test_data2(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = test_data2(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% % For third Image
% imageCount = size(test_data3,1)/36
% totalCountTest = imageCount + totalCountTest;
% pred = net(test_data3(:,1:end-1)')
% predicted = vec2ind(pred);
% for i = 1: imageCount
%     actualImage = test_data3(i*36,end)
%     vectPred = predicted(36*(i-1)+1:36*i)
%     ratio = [size(find(vectPred == 1),2)/36 ; size(find(vectPred == 2),2)/36 ; size(find(vectPred == 3),2)/36]
%    [~ ,predImage] = max(ratio)
%    if (actualImage == predImage )
%        count = count + 1;
%    end
% end
% 
% accuracy_test = count / totalCountTest
% 
% %% To calculate output of hidden layer
% 
% % 
% % b1 = net.b{1}
% % b2 = net.b{2}
% % b3 = net.b{3}
% % IW = net.IW{1,1} 
% % LW_1 = net.LW{2,1}
% % LW_2 = net.LW{3,2}
% % 
% % %net.layers{1}.transferFcn % To know the function used for activation 
% % 
% % o_p_h1 = tansig( IW*X_Train' + b1 * ones(1,size(X_Train,1) ) );
% % o_p_h2 = tansig(LW_1*o_p_h1 + b2*ones(1,size(o_p_h1,2)))
% % o_p_final = softmax(LW_2*o_p_h2 + b3*ones(1,size(o_p_h2,2)))
% 
% %%  
% function[noOfImage , fetVectInEachImage, data] = thedata(class)
% 
% relativePath = '/../../../data_assign3_group18/dataset3/features/';
% relativePath = strcat(relativePath,class,'/')
% structOfFiles = dir(strcat(pwd,relativePath))
% 
% filePath=[]
% data = []
% filePath = string(filePath)
% fetVectInEachImage =[];
% noOfImage =0;
% for i = 3:size(structOfFiles,1)
%     noOfImage = noOfImage+1;
%     filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)
%     tempD = load( filePath(i))
%     data = [data; tempD]
%     fetVectInEachImage =[fetVectInEachImage ; size(tempD,1)];
% end
% 
% label = 0
% switch(class)
%     case 'highway'
%         label = 1
%     case 'street'
%         label = 2
%     case 'tb'
%         label = 3
% end
% data(:,end+1) = ones(size(data,1),1)*label 
% end
