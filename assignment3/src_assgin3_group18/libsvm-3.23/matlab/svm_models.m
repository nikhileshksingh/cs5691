% 
% Examples of options: -s 0 -c 10 -t 1 -g 1 -r 1 -d 3 
% Classify a binary data with polynomial kernel (u'v+1)^3 and C = 10
% 
%  
% options:
% -s svm_type : set type of SVM (default 0)
% 	0 -- C-SVC
% 	1 -- nu-SVC
% 	2 -- one-class SVM
% 	3 -- epsilon-SVR
% 	4 -- nu-SVR
% -t kernel_type : set type of kernel function (default 2)
% 	0 -- linear: u'*v
% 	1 -- polynomial: (gamma*u'*v + coef0)^degree
% 	2 -- radial basis function: exp(-gamma*|u-v|^2)
% 	3 -- sigmoid: tanh(gamma*u'*v + coef0)
% -d degree : set degree in kernel function (default 3)
% -g gamma : set gamma in kernel function (default 1/num_features)
% -r coef0 : set coef0 in kernel function (default 0)
% -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
% -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
% -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
% -m cachesize : set cache memory size in MB (default 100)
% -e epsilon : set tolerance of termination criterion (default 0.001)
% -h shrinking: whether to use the shrinking heuristics, 0 or 1 (default 1)
% -b probability_estimates: whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
% -wi weight: set the parameter C of class i to weight*C, for C-SVC (default 1)
% 
% The k in the -g option means the number of attributes in the input data



%% Load data {'2_linear', '2_non_linear' }
clc;
clear; 


%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data] = load_data('2_non_linear')

% x1_MAX = max(train_data(:,1))
% x2_MAX = max(train_data(:,2))
% x1_MIN = min(train_data(:,1))
% x2_MIN = min(train_data(:,2))
% 
% max_min = [x1_MAX x1_MIN; x2_MAX x2_MIN]

X_Train = train_data(:,1:end-1); 


Y_Train = train_data(:,end); 

X_Test  = test_data(:,1:end-1); 


Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);

Y_Val = val_data(:, end);

%X_Train = normalize(X_Train,max_min)
%X_Test = normalize(X_Test,max_min)
%X_Val = normalize(X_Val,max_min)
%%
training_label_vector = Y_Train
training_instance_matrix = X_Train




test_label = Y_Val
test_data = X_Val

%% Linear kernel

model_linear = svmtrain(training_label_vector, training_instance_matrix , '-t 0 -c 0.1')
[predict_label_L, accuracy_L, dec_values_L] = svmpredict(test_label, test_data, model_linear);

%% Polynomial Kernel

model_poly = svmtrain(training_label_vector, training_instance_matrix , '-t 1 -c 3 -r 5 -d 3')
[predict_label_P, accuracy_P, dec_values_P] = svmpredict(test_label, test_data, model_poly);

%% Gaussian Kernel

model_gaussian = svmtrain(training_label_vector, training_instance_matrix , '-t 2 -c 10 -g 0.35 ')
[predict_label_G, accuracy_G, dec_values_G] = svmpredict(test_label, test_data, model_gaussian);



%%

% xrange = -10:0.2:20
% yrange = -15:0.15:20

% xrange = 0:0.005:1
% yrange = 0:0.005:1

xrange = -2:0.02:3
yrange = -1.5:0.02:1.5

% xrange = -6:0.2:10
% yrange = -15:0.3:15

t = [training_instance_matrix Y_Train]
decision_surface(t, xrange, yrange, model_gaussian)




%%
function decision_surface(train_data, xrange, yrange, model)
   
    [x , y] = meshgrid(xrange,yrange);
    mydata = [x(:) y(:)]

    

    [prediction, ~ , ~] =  svmpredict(mydata(:,1), mydata, model);

    

    mydata = [mydata prediction]
    figure;
    hold on
    for j = 1 : size(mydata,1)
        if(mydata(j,end) ==1)
            plot(mydata(j,1), mydata(j,2),'.r')
        else if (mydata(j,end) == 2)
            plot(mydata(j,1), mydata(j,2),'.b')
        else if (mydata(j,end) == 3)
            plot(mydata(j,1), mydata(j,2),'.m')        
        else 
            plot(mydata(j,1), mydata(j,2),'.g')
            end
            end
        end
    end
    nClasses = max(train_data(:,3))
    points = size(train_data,1)/nClasses
    
    plot(train_data(1:points,1 ), train_data(1:points,2), 'k+','LineWidth', 2, ...
'MarkerSize', 5)

    plot(train_data(points+1:2*points,1 ), train_data(points+1:2*points,2),  'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 5)


    if (nClasses == 3)
        plot(train_data(2*points+1:end,1), train_data(2*points+1:end,2), '^k','LineWidth', 1, ...
'MarkerSize', 5) 
    end
    
    plot(train_data(model.sv_indices,1),train_data(model.sv_indices,2),  'kd', 'LineWidth', 1, 'MarkerFaceColor', 'g', 'MarkerSize', 7)
    
    
    %scatter(train_data(model.sv_indices,1),train_data(model.sv_indices,2), 'd', 'filled', 'MarkerEdgeColor','k',...
       % 'MarkerFaceColor',[0 1 0], 'MarkerSize', 7)
    %plot(test_data(:,1), test_data(:,2),'+')
    %plot(val_data(:,1), val_data(:,2),'O')
    hold off

end

%% Normalize

function x_normed = normalize(data,max_min)

noF = size(data,2)


tmp1 = (data(:,1) - max_min(1,2)) / (max_min(1,1) - max_min(1,2))
tmp2 = (data(:,2) - max_min(2,2)) / (max_min(2,1) - max_min(2,2))

x_normed = [tmp1 tmp2]
end
