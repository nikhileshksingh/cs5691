%% Function for polynomial basis function

function out = polybasis(X, degree)

X1 = X(:,1)
X2 = X(:,2)
out = ones(size(X1(:,1)));
for i = 1:degree
    for j = 0:i
        out(:, end+1) = (X1.^(i-j)).*(X2.^j);
    end
end
end