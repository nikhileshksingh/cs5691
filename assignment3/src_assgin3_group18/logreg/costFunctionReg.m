%% Function to compute the cost function

function [J, grad] = costFunctionReg(w, X, y, lambda)

N = length(y); 
J = 0;
grad = zeros(size(w));


temp_w = w;
temp_w(1) = 0;

J = (-1 / N) * sum(y.*log(logistic_fn(X * w)) + (1 - y).*log(1 - logistic_fn(X * w))) + (lambda / (2 * N))*sum(temp_w.^2);
temp = logistic_fn (X * w);
error = temp - y;
grad = (1 / N) * (X' * error) + (lambda/N)*temp_w;

end