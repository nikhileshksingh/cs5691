%% Function for calculating PHI using K means clusttering
function [PHI ] = gaussianbasis(data , clustMean , variance ,kval)
kMeans = clustMean
k= kval
x = data(:,1:end)
PHI = []
for i = 1:k
   
        meanRepeat = repmat(kMeans(i,:),size(x,1),1)
        dist = meanRepeat - x
        sqEucledianNorm = vecnorm(dist,2,2).^2
        colPhiElement  =     exp(-sqEucledianNorm/(2*variance))
        PHI =[PHI colPhiElement]   
end
PHI = [ones(size(x,1),1) PHI]

end
