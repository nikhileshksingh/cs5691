

%% Initialization
clear ; close all; clc

%  The first two columns contains the exam scores and the third column
%  contains the label.
clc;
clear; 

%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data] = load_data('2_non_linear')

X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 

X_Test  = test_data(:,1:end-1);    
Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);


data = train_data
X = data(:, [1, 2]); y = data(:, 3);
y = y-1;

%% Polynomial Basis Functions

degree = 3
X = polybasis(X_Train, degree); %Basis Functions
PHI_Test = polybasis(X_Test,degree);
PHI_Val = polybasis(X_Val,degree)
initial_w = zeros(size(X, 2), 1);
lambda = 0.1;

% Set Options
options = optimset('GradObj', 'on', 'MaxIter', 400);

[w, J, exit_flag] = fminunc(@(t)(costFunctionReg(t, X, y, lambda)), initial_w, options);

g_Train = predict_bin(w, X);
g_Test = predict_bin(w,PHI_Test)
g_Val = predict_bin(w,PHI_Val)
fprintf('Train Accuracy: %f\n', mean(double(g_Train == y)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(g_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(g_Test == (Y_Test-1))) * 100);

w_poly = w;

%% Gaussian Basis Functions

K = 10

[idx, kM] = kmeans(X_Train,K)
var = returnVar(X_Train, idx, kM)

[id, k] = kmeans(X_Test,K)
var_Test = returnVar(X_Test, id, k)

[id, k] = kmeans(X_Val,K)
var_Val = returnVar(X_Val, id, k)

X = gaussianbasis(X_Train , kM , var,K)
PHI_Test = gaussianbasis(X_Test,kM, var_Test,K);
PHI_Val = gaussianbasis(X_Val,kM,var_Val,K)


initial_w = zeros(size(X, 2), 1);
% Set Options
options = optimset('GradObj', 'on', 'MaxIter', 400);

[w, J, exit_flag] = fminunc(@(t)(costFunctionReg(t, X, y, lambda)), initial_w, options);


g_Train = predict_bin(w, X);
g_Test = predict_bin(w,PHI_Test)
g_Val = predict_bin(w,PHI_Val)
fprintf('Train Accuracy: %f\n', mean(double(g_Train == y)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(g_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(g_Test == (Y_Test-1))) * 100);

w_gauss = w;

%% Decision region

xrange = -2:0.03:3
yrange = -1.5:0.02:1.5

[x , y] = meshgrid(xrange,yrange);
mydata = [x(:) y(:)]

[id, k] = kmeans(X_Test,K)
var_PHI = returnVar(mydata, id, k)
%%
var_PHI = 0.2
gauss_X = gaussianbasis(mydata , kM , var_PHI, K)
poly_X = polybasis(mydata, degree)


%decision_surface(train_data, gauss_X, poly_X, w_poly,w_gauss, mydata, 'poly', 'bin')

decision_surface(train_data, gauss_X, poly_X, w_poly,w_gauss, mydata, 'gaussian', 'bin')









