%% Decision Region Function




function decision_surface(train_data, gauss_X, poly_X, w_poly, w_gauss, mydata, model, classes)
   


    switch model
        case 'gaussian'
        switch classes
            case 'bin'
                % w_poly and w_gauss contain weights
          prediction = predict_bin(w_gauss,gauss_X)
          
            case 'mul'
                % w_poly and w_gauss contain model_poly and model_gauss
                [p, ~] = predict_mul(w_gauss, gauss_X')
                prediction = p'
                
        end
        case 'poly'
        switch classes
            case 'bin'
          prediction = predict_bin(w_poly,poly_X)
          
            case 'mul'
          [p, ~] = predict_mul(w_poly, poly_X')
          prediction = p'
                
        end
          
    end
    
     mydata = [mydata prediction]
   
    figure;
    hold on
    for j = 1 : size(mydata,1)
        if(mydata(j,end) == 0)
            plot(mydata(j,1), mydata(j,2),'.r')
        else if (mydata(j,end) == 1)
            plot(mydata(j,1), mydata(j,2),'.b')
        else if (mydata(j,end) == 2)
            plot(mydata(j,1), mydata(j,2),'.m')        
        else 
            plot(mydata(j,1), mydata(j,2),'.g')
            end
            end
        end
    end
    nClasses = max(train_data(:,3))
    points = size(train_data,1)/nClasses
    
    
    plot(train_data(1:points,1 ), train_data(1:points,2), 'k+','LineWidth', 2, ...
'MarkerSize', 5)
    plot(train_data(points+1:2*points,1 ), train_data(points+1:2*points,2),'ko', 'MarkerFaceColor', 'y', ...
'MarkerSize', 5)


    if (nClasses == 3)
        plot(train_data(2*points+1:end,1), train_data(2*points+1:end,2), '^k' )
    end


    %plot(train_data(:,1), train_data(:,2),'*k')
    
    
    
    %scatter(train_data(model.sv_indices,1),train_data(model.sv_indices,2), 'd', 'filled', 'MarkerEdgeColor','k',...
      %  'MarkerFaceColor',[0 1 0])
    %plot(test_data(:,1), test_data(:,2),'+')
    %plot(val_data(:,1), val_data(:,2),'O')
    hold off

end






