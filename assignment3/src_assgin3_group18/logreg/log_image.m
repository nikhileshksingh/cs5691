
clc;
clear; 



% [noOfImage1, fetVectSize1 , highway] = thedata('highway')
% [noOfImage2, fetVectSize2 ,street] = thedata('street')
% [noOfImage3, fetVectSize3 ,tb] = thedata('tb')

noOfImage1 = 260
noOfImage2 = 292
noOfImage3 = 356
load('image_data.mat')
%%
%data = [highway; street; tb]

%data(:,1:end-1) = normalize(data(:,1:end-1),2,'range',[-1 1])

%%
%  dat = data(:,1:end-1)
%  % De-mean
% dat = bsxfun(@minus,dat,mean(dat));
%  %%
% 
% [coeff,score,latent] = pca(dat)
% data = [score(:,1:10) data(:,end)]
%%

[train_ind val_ind test_ind] = dividerand(noOfImage1+noOfImage2+noOfImage3, 0.7, 0.2,0.1)

train_d_ind = [];
for i = train_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
    train_d_ind =[train_d_ind [startindx:endindx] ];
end
train_data = data(train_d_ind,:) 

val_d_ind =[];

for i = val_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
    val_d_ind =[val_d_ind [startindx:endindx] ];     
end
val_data = data(val_d_ind,:)


test_d_ind =[];

for i = test_ind
    startindx = 36*(i-1)+1
    endindx = startindx + 36 -1
    test_d_ind =[test_d_ind [startindx:endindx] ];     
end
test_data = data(test_d_ind,:)


%%



X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 
Y_Train = Y_Train-1;

X_Test  = test_data(:,1:end-1);    

Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);


%% Polynomial Basis Function
 
degree = 10

X = polybasis(X_Train, degree); %Basis Functions
PHI_Test = polybasis(X_Test,degree);
PHI_Val = polybasis(X_Val,degree)

[model_poly, llh] = logitMn(X', Y_Train')

[p_Train, P] = predict_mul(model_poly, X')
[p_Test, P] = predict_mul(model_poly, PHI_Test')
[p_Val, P] = predict_mul(model_poly, PHI_Val')

p_Train = p_Train'-1
p_Test = p_Test'-1
p_Val = p_Val'-1



fprintf('Train Accuracy: %f\n', mean(double(p_Train == Y_Train)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(p_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(p_Test == (Y_Test-1))) * 100);

%%
imageCount = size(X_Test,1)/36
% predicted = vec2ind(y);
count =0;
for i = 1: imageCount
    actualImage = Y_Test(i*36)-1;
    
    ratio = [size(find(p_Test == 0),1)/36 ; size(find(p_Test == 1),1)/36 ; size(find(p_Test == 2),1)/36];
   [~ ,predImage] = max(ratio)
   if (actualImage == (predImage-1))
       count = count + 1;
   end
end
test_accuracy  = count/imageCount






%% Gaussian Decision Surface

K = 20
[idx, kM] = kmeans(X_Train,K)
var = returnVar(X_Train, idx, kM)

[id, k] = kmeans(X_Test,K)
var_Test = returnVar(X_Test, id, k)

[id, k] = kmeans(X_Val,K)
var_Val = returnVar(X_Val, id, k)


X = gaussianbasis(X_Train,kM , var,K);    %Basis Functions
PHI_Test = gaussianbasis(X_Test,kM , var_Test,K); 
PHI_Val = gaussianbasis(X_Val,kM , var_Val,K); 

[model_gauss, llh] = logitMn(X', Y_Train')

[p_Train, P] = predict_mul(model_gauss, X')
[p_Test, P] = predict_mul(model_gauss, PHI_Test')
[p_Val, P] = predict_mul(model_gauss, PHI_Val')

p_Train = p_Train'-1
p_Test = p_Test'-1
p_Val = p_Val'-1

fprintf('Train Accuracy: %f\n', mean(double(p_Train == Y_Train)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(p_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(p_Test == (Y_Test-1))) * 100);


%%
imageCount = size(X_Test,1)/36
% predicted = vec2ind(y);
count =0;
for i = 1: imageCount
    actualImage = Y_Test(i*36)-1;
    
    ratio = [size(find(p_Test == 0),1)/36 ; size(find(p_Test == 1),1)/36 ; size(find(p_Test == 2),1)/36];
   [~ ,predImage] = max(ratio)
   if (actualImage == (predImage-1))
       count = count + 1;
   end
end
test_accuracy  = count/imageCount


%% Decision Surface

xrange = -10:0.15:20
yrange = -15:0.2:20

[x , y] = meshgrid(xrange,yrange);
mydata = [x(:) y(:)]

[id, k] = kmeans(X_Test,K)
var_PHI = returnVar(mydata, id, k)

gauss_X = gaussianbasis(mydata , kM , var_PHI, K)
poly_X = polybasis(mydata, degree)



decision_surface(train_data, gauss_X, poly_X, model_poly, model_gauss, mydata, 'poly', 'mul' )

%decision_surface(train_data, gauss_X, poly_X, model_poly, model_gauss, mydata, 'gaussian', 'mul')







