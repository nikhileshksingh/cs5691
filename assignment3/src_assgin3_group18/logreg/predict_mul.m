%% Function to predict

function [y, P] = predict_mul(model, X)
W = model.W;
X = [X; ones(1,size(X,2))];
P = softmax(W'*X);
[~, y] = max(P,[],1);
end