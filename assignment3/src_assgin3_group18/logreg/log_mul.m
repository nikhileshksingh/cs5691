
%% Load data {'2_linear', '2_non_linear' }
clc;
clear; 


%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data] = load_data('2_linear')


X_Train = train_data(:,1:end-1); 
Y_Train = train_data(:,end); 
Y_Train = Y_Train-1;

X_Test  = test_data(:,1:end-1);    

Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1);
Y_Val = val_data(:, end);


%% Polynomial Basis Function
 
degree = 1

X = polybasis(X_Train, degree); %Basis Functions
PHI_Test = polybasis(X_Test,degree);
PHI_Val = polybasis(X_Val,degree)

[model_poly, llh] = logitMn(X', Y_Train')

[p_Train, P] = predict_mul(model_poly, X')
[p_Test, P] = predict_mul(model_poly, PHI_Test')
[p_Val, P] = predict_mul(model_poly, PHI_Val')

p_Train = p_Train'-1
p_Test = p_Test'-1
p_Val = p_Val'-1

fprintf('Train Accuracy: %f\n', mean(double(p_Train == Y_Train)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(p_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(p_Test == (Y_Test-1))) * 100);




%% Gaussian Decision Surface

K = 10
[idx, kM] = kmeans(X_Train,K)
var = returnVar(X_Train, idx, kM)

[id, k] = kmeans(X_Test,K)
var_Test = returnVar(X_Test, id, k)

[id, k] = kmeans(X_Val,K)
var_Val = returnVar(X_Val, id, k)


X = gaussianbasis(X_Train,kM , var,K);    %Basis Functions
PHI_Test = gaussianbasis(X_Test,kM , var_Test,K); 
PHI_Val = gaussianbasis(X_Val,kM , var_Val,K); 

[model_gauss, llh] = logitMn(X', Y_Train')

[p_Train, P] = predict_mul(model_gauss, X')
[p_Test, P] = predict_mul(model_gauss, PHI_Test')
[p_Val, P] = predict_mul(model_gauss, PHI_Val')

p_Train = p_Train'-1
p_Test = p_Test'-1
p_Val = p_Val'-1

fprintf('Train Accuracy: %f\n', mean(double(p_Train == Y_Train)) * 100);
fprintf('Val Accuracy: %f\n', mean(double(p_Val == (Y_Val-1))) * 100);
fprintf('Test Accuracy: %f\n', mean(double(p_Test == (Y_Test-1))) * 100);


%% Decision Surface

xrange = -10:0.15:20
yrange = -15:0.2:20

[x , y] = meshgrid(xrange,yrange);
mydata = [x(:) y(:)]

[id, k] = kmeans(X_Test,K)
var_PHI = returnVar(mydata, id, k)

gauss_X = gaussianbasis(mydata , kM , var_PHI, K)
poly_X = polybasis(mydata, degree)



decision_surface(train_data, gauss_X, poly_X, model_poly, model_gauss, mydata, 'poly', 'mul' )

%decision_surface(train_data, gauss_X, poly_X, model_poly, model_gauss, mydata, 'gaussian', 'mul')







