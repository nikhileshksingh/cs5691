%% Function to return variance

function variance = returnVar(data, idx, kM)

K = size(kM,1)
sum_v = 0
for i = 1:K
    tmpvarvec = var(data(idx==i,:))
    tmpvar = sum(tmpvarvec)/size(tmpvarvec,2)
    sum_v = sum_v + tmpvar

end
variance = sum_v/K

end
