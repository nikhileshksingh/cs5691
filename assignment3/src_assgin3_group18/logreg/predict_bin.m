%% Function to predict

function p = predict_bin(theta, X)

N = size(X, 1);
p = zeros(N, 1);
p = round(logistic_fn(X * theta));

end