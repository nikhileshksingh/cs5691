


%% Softmax function 
function [Y,s] = softmax(X, dim)



if nargin == 1
    dim = find(size(X)~=1,1);
    if isempty(dim), dim = 1; end
end
s = logsumexp(X,dim);
Y = exp(X-s);
