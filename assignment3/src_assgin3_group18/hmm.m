

%% Reading data
clear
relativePath = '/../data_assign3_group18/dataset1/1a_ocr_dataset/HandWritten_data/FeaturesHW/';
structOfFiles = dir(strcat(pwd,relativePath))

filePath=[]

filePath = string(filePath)
for i = 3:5
    filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)
end

[ai, ~] = load_data(filePath(3))
[ba, ~] = load_data(filePath(4))
[la, ~] =load_data(filePath(5))

aix = returncoord(ai)
bax = returncoord(ba)
lax = returncoord(la)

myx = [aix(:,1:2); bax(:,1:2); lax(:,1:2)]
%%
center = (min(myx)+max(myx))/2
myx = myx - center

%% K-Means on myx
K = 10

idx = kmeans(myx,K)
idx = idx-1
%%

ai_ind = idx(1:8652, :)
ba_ind = idx((8652+1):8652+7182 , :)
la_ind = idx(8652+7183:end , :)

aif = [aix ai_ind]
baf = [bax ba_ind]
laf = [lax la_ind]
%%



writefile(aif,'aif_k10.txt')
writefile(baf,'baf_k10.txt')
writefile(laf,'laf_k10.txt')
%%

%%

[A1,B1, pi_vec1] = returnModel('aif_k15_s15_train.hmm')
[A2,B2, pi_vec2] = returnModel('baf_k15_s15_train.hmm')
[A3,B3, pi_vec3] = returnModel('laf_k15_s15_train.hmm')


%%

datafile = load_data('aif_k15.txt_val')
file = 2

labels =[]
prob2 = []
prob3 = []
for loop = 1:size(datafile,1)
    probVec = []
    temp = datafile(loop,find(~cellfun(@isempty,datafile(loop,:))))
    temp = cell2mat(temp)
    temp = temp+1
    
    probVec = [ pr_hmm(temp,A1,B1,pi_vec1) pr_hmm(temp,A2,B2,pi_vec2)  pr_hmm(temp,A3,B3,pi_vec3)  ]
    [val,indx] = max(probVec)
    labels = [labels; indx]
end


accuracy = size(find(labels==file),1)/size(labels,1)

%prob = pr_hmm(o,A,B,pi_vec)

% function prob = forward_hmm(o,A,B,pi_vec)
%     
% 
% end



%% Probability calculation


function [A,B, pi_vec] = returnModel(charFile)
[model, ~] = load_data(charFile)

model = cell2mat(model)
nstates = size(model,1)/2
nsymbols = size(model,2)-1

A = zeros(nstates)
B = zeros(nstates, nsymbols)
pi_vec = zeros(nstates,1)

trans = model(:,1)
A(1,1) = trans(1)

symb = model(:,2:nsymbols+1)
symb = trans.*symb
trans = reshape(trans,[2,nstates])

for i = 1:nstates
    
    A(i,i) = trans(1,i)
    A(i,i+1) = trans(2,i)
end
A = A(:,1:end-1)

for i = 0:nstates-1
    B(i+1,:) = symb(2*i+1,:) + symb(2*i+2,:)
 end

% for i = 1:2:nstates
%    B(i,:) = symb(i,:) 
% end

prob_for_pi = 1/nstates
pi_vec = ones(nstates,1)*prob_for_pi

end


%%
    function   writefile(final,filename )
     
        fileID = fopen(filename,'w')
       for i = 1 :final(end,3)  
          aa = final(find(final(:,3)==i),4)';
          fprintf(fileID,'%i ',aa);
          fprintf(fileID,'\n');
       end
            fclose(fileID);
    end
   

%%
function z = returncoord(cellarray)
z= []
tmp = []
count = 1
for row = 3:3:size(cellarray ,1)
    x = []
    y= []
    tmp = []
    nc = cellarray{row,1}
    nc = (nc*2)+1;
    coly = 3:2:nc;
    colx = coly -1;
    x =  [x; cellarray{row,colx}];
    y =  [y; cellarray{row,coly}];
    
    tmp = [x' y' (ones(size(x',1),1)*count)];
    z = [z; tmp]
    count = count + 1
end
 
end
%%
function p=pr_hmm(o,a,b,pi)

n=length(a(1,:));
T=length(o);
%Forward Method
for i=1:n        %inital step
    m(1,i)= b(i,o(1))*pi(i);
end
for t=1:(T-1)      %recursion
    for j=1:n
        z=0;
        for i=1:n
            z=z+a(i,j)*m(t,i);
        end
        m(t+1,j)=z*b(j,o(t+1));
    end
end
p=0;
for i=1:n         %final
    p=p+m(T,i);        
end
p = log(p)
end

%%
% for i = 1:292
% if (mod (i,3) == 1)
%     if (A{1,i} ~= 1)
%         disp("Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
%         break
%     else
%         disp("Done!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
%     end    
% end
% end
%% 

function [ca,ca2] = load_data(filePath)
fid = fopen(filePath);
textLine = fgets(fid); % Read first line.
lineCounter = 1;
while ischar(textLine)
    fprintf('\nLine #%d of text = %s\n', lineCounter, textLine);
  % get into numbers array.
  numbers = sscanf(textLine, '%f ')
  % Put numbers into a cell array IF and only if
  % you need them after the loop has exited.
  % First method - each number in one cell.
  for k = 1 : length(numbers)
    ca{lineCounter, k} = numbers(k);
  end
  % ALternate way where the whole array is in one cell.
  ca2{lineCounter} = numbers;
  
  % Read the next line.
    textLine = fgets(fid);
  lineCounter = lineCounter + 1;
end
fclose(fid);
end




