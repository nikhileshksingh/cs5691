
function [predictClass]= knn_classifier(traindata, testdata, K)


%Find distance with all training datapoints, sort and poll
for i = 1 : size(testdata)
x = testdata(i,:);

% dist = zeros(size(traindata,1),1) ; 
% for j = 1:(size(traindata,2)-1)
%     dist = dist +   (traindata(:, j) - x(j)) .^ 2
% end
% dist = sqrt(dist)
dist = sqrt((traindata(:, 1) - x(1)) .^ 2 + (traindata(:, 2) - x(2)) .^ 2);

classes = traindata(:, end);
dist(:, 2) = classes;
poll = sortrows(dist, 1);

%For tiebreak in case of even K
if (mod(K, 2) == 1)
expclass(i) = mode(poll(1 : K, 2));
else
    temp = poll(1 : K, 2);
    uniq = unique(temp);
    p = size(uniq);
    bincounts = histc(temp, uniq);
    q = max(bincounts);
  
    M = (p == 2) & (q == K/2);
  
    expclass(i) = mode(poll(1 : K - M, 2));    
end
end
predictClass = expclass;
%Error percentage calculation
%error = transpose(expclass) - testdata(:, end);
%accur = ((size(error, 1) - nnz(error))/size(error, 1));

end
