function   [accuracy]= GMMClassifierVarLenPredict( field,TestSample,TestPredict,datacellTrain ,datacellTest, K )  
%field is saved model


NoClass = size(datacellTest,1)
truelabel =[]
predictlabel =[]

classPrior = [0.1478 0.1214 0.1214 0.0985 0.5109]


for eachClass = 1: size(datacellTest,1)
    for eachImage = 1: size(datacellTest,2)
        if(~isempty(datacellTest{eachClass,eachImage}))
            %%% Yaha se karna hai
            truelabel = [ truelabel eachClass]
            matrixImage = datacellTest{eachClass,eachImage}
            N =size(matrixImage,1)
            D = size(matrixImage,2)
            gfinal = []
            for Idx_test = 1:  N   
                x = matrixImage(Idx_test,:)';  % NoF*1
                g=zeros(NoClass,K);
                
                
                for i= 1: NoClass
                    
                    Mu   =field.para(i)mu;
                    %Mu   =Mu'; %temprory
                    Sigma = field.para(i).Sigma;
                    P_w  = field.para(i).Pi;  
                    
                    for j=1:K
                        
                    
                        xshift =  x-Mu(:,j)
                        inv_pSigma = inv(Sigma(:,:,j));
                        tmp = xshift'*inv_pSigma*xshift
                        
                        coef  = (2*pi)^(D/2) * sqrt(det(inv_pSigma)); 
                        probab = (coef^(-1)) * exp(-0.5*tmp) 
                        
                        
                        g(i,j)= P_w(j)*probab
                        
                        
                    end
                    gfinal(Idx_test,i)= sum(g(i,:))
                    
                end
                
            end
            gfinalFinal = prod(gfinal,1) 
            gfinalFinal = gfinalFinal.*classPrior
            [~,indxx] = max(gfinalFinal)
            predictlabel = [ predictlabel indxx]
        end
        
    end
    
end
accuracy =return_accuracy_matrix(truelabel, predictlabel)


 
end  


