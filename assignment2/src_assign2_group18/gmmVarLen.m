function model = gmmVarLen(X, K)
%% Generate Initial Centroids
    threshold = 1e-14;
    maxiter = 500;
    
    [N, D] = size(X);
 
        
        [~,centroids,~,DistMatrix] = kmeans(X,K);
        
    [pMiu,pPi,pSigma] = init_params();
 
    Lprev = -inf; 
    
    %% EM Algorithm
    for iter = 2:maxiter
        
        Px = real(calc_prob());    
        Px(isnan(Px))=0;
        Px(isnan(Px))=0;
       
        %pgamma is v_nq in our notes
        pGamma = Px .* repmat(pPi, N, 1); 
        pGamma = pGamma ./ repmat(sum(pGamma, 2), 1, K);
        
       
        
        Nk = sum(pGamma, 1);    
        Nk(isnan(Nk))=0;
        Nk(isinf(Nk))=0;
        
       
        pMiu = diag(1./Nk) * pGamma' * X; 
        pPi = Nk/N;
        
        
        
        for kk = 1:K
            sumn =zeros(D,D)
            for t = 1 : size(X,1)
                    Xshift = X(t,:)-pMiu(kk, :);
                    sumn = sumn + (pGamma(t, kk)*(Xshift'*Xshift))
                    
            end
            pSigma(:, :, kk) =        sumn/Nk(kk)
        end
 
        % check for convergence
        L = sum(log(Px*pPi'));
        if L-Lprev < threshold  
            break;
        end
        Lprev = L;
        
    end
 
 
        model = [];
        model.Miu = pMiu';
        model.Sigma = pSigma;
        model.Pi = pPi;
 

 
    
    function [pMiu,pPi,pSigma] = init_params()
        pMiu = centroids; 
        pPi = zeros(1, K); 
        pSigma = zeros(D, D, K); 
 
       
 
        [~,labels] = min(DistMatrix,[],2);
        
        for k=1:K
            Xk = X(labels == k, :);
            pPi(k) = size(Xk, 1)/N; 
            pSigma(:, :, k) = cov(Xk);
        end
    end
 
    function Px = calc_prob() 
        Px = zeros(N, K);
        for k = 1:K
            
            for r = 1:size(X,1)
               xshift =  X(r,:)-pMiu(k, :)
               inv_pSigma = inv(pSigma(:, :, k));
               tmp = xshift*inv_pSigma*xshift'
               coef  = (2*pi)^(D/2) * sqrt(det(inv_pSigma)); 
               Px(r, k) = (coef^(-1)) * exp(-0.5*tmp); 
            end
            
            
        end
    end
end
