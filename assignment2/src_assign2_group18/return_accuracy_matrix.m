% accuracy and confusion matrix
function [accur] =  return_accuracy_matrix(Y_target, Y_predicted)

    targets=ind2vec(Y_target');
    outputs=ind2vec(Y_predicted');
    figure;
    plotconfusion(targets,outputs);
    
    err = Y_target - Y_predicted
    accur = 1 - nnz(err)/size(Y_target,1);
end