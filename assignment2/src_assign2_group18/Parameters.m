
% parameters of gausian distributions
function [Mu, C] = Parameters(X)

    [NoS,NoF] =size(X);
    sum_X = zeros(1,NoF); 

    for i = 1:NoS
        sum_X = sum_X + X(i,:);
    end
    Mu = sum_X ./ NoS;

    
    sum_X = zeros(NoF,NoF); 
    for i = 1:NoS
        sum_X = sum_X + (X(i,:)-Mu)'*(X(i,:)-Mu);
    end
    C = sum_X ./ NoS;

    
   




    
