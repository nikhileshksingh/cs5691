% Function for bayes knn 
% 1_6 and 2_3 in assignment

function pred = bayes_knn(train_data,X_Val, K)


d = size(train_data,2)-1
Y_Train = train_data(:,end)
NoC = max(Y_Train)

ind = []
for i=1:NoC
   ind = [ind; nnz(Y_Train == i)];
end
ind = cumsum(ind)

X_Train = train_data(:,1:end-1);


%prior = return_priors(Y_Train)


pred=[]
for i=1:size(X_Val,1)
    V = []
    for p=1:NoC
       
    if(p==1)    
    xt = X_Train(1:ind(p),:);
    else
        xt = X_Train((ind(p-1)+1):ind(p),:);
    end
    
    %find K nearest neighbours of X_Val(i,:) in train_data
    distVec = vecnorm(xt - X_Val(i,:), 2,2) ;
    [B,I] = sort(distVec,'ascend') ;
    %radius of the hyper-sphere
    r = B(K)
    V =  [V returnVol(r,d)];
    end
    [~,i_star] = min(V);
    pred = [pred; i_star]
    
end
    

end

function vol = returnVol(r,d)

if (mod(d,2) == 0)
   vol = (((pi)^(d/2))/(factorial(d/2))) * (r^d)
    
    
else
    vol = ( (2^d)*((pi)^((d-1)/2))*(factorial((d-1)/2)) / (factorial(d)) ) * (r^d)
    
end


end

function prior = return_priors(Y_Train)

N = size(Y_Train,1)
prior = []
for i=1:max(Y_Train)
    prior = [prior; ( (nnz(Y_Train == i) )/ N)]
end


end

