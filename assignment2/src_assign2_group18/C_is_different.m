% Function when covariance metrices are different
% 2_c and 3b in 1st part
function [Y_Test] = C_is_different(classifier, X_Train, Y_Train, X_Test)


[~  ,NoTrS] = size(X_Train); 
[NoF,NoTeS] = size(X_Test);
Y_Test=zeros(NoTeS,1);


class_label = min(Y_Train); 
i = 0; 
class = zeros(max(Y_Train),2);
for j= 1: max(Y_Train)
       class_temp = find( Y_Train == class_label); 
       i = i+1;  
       class(i,1)= class_label  ;    
       class(i,2)= length(class_temp );
       class_label = class_label+1;   
end 

class(class(:,2)==0,:)=[]; 
NoC = length(class);

Mu = zeros(NoF,NoC); 
C = zeros(NoF,NoF,NoC);
P_w = zeros(NoC,1);   

for i = class(:,1)'    
     Index_class = (Y_Train == i); 
     X_Train_classi = (X_Train(:,Index_class))';  
     
     [Mu_temp, C(:,:,i)] = Parameters(X_Train_classi);
  
     Mu(:,i)= Mu_temp';
     P_w(i)=  class(i,2)/ NoTrS; 
     
end

% Diagonal element being zero for naive part

if (strcmp(classifier,'naive'))
for c = 1:NoC
    for i = 1:NoF
        for j = 1:NoF
            if(i~=j)
                C(i,j,c) = 0
            end
        end
    end
end
end



    g = zeros(NoC,1) ;
    warning('off','all')
    for Idx_test = 1:(NoTeS)    
        x = X_Test(:,Idx_test); 
        for i = 1:NoC      
            %g(i)= -0.5* (x - Mu(:,i))'*(Sigma(:,:,i)^(-1))*(x - Mu(:,i))- NoF/2*log(2*pi)-0.5*log(det(Sigma(:,:,i)))+log(P_w(i));
           
            %g(i)= -0.5*( (2*log(2*pi)) + (log((Sigma(:,:,i)) + (x -Mu(:,i))'*(1/((Sigma(:,:,i)))*(x-Mu(:,i))))))

            g(i)= -0.5* (x - Mu(:,i))'*(C(:,:,i)^(-1))*(x - Mu(:,i))- NoF/2*log(2*pi)-0.5*log(det(C(:,:,i)))+log(P_w(i));
      
        end
       %-----------------------------------------------------------------------
      [maxg,Idx_maxg]= max(g);
        
           Y_Test(Idx_test) = Idx_maxg; 
 
    end 
 
