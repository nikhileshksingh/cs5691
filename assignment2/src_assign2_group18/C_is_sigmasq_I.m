% Function when covariance metrix sigma*I
% % 2_a 
function [Y_Test] = C_is_sigmasq_I(X_Train, Y_Train, X_Test)


[~  ,NoTrS] = size(X_Train); 
[NoF,NoTeS] = size(X_Test);
Y_Test=zeros(NoTeS,1);

class_label = min(Y_Train); 
i = 0; 
class = zeros(max(Y_Train),2);
for j= 1: max(Y_Train)
       class_temp = find( Y_Train == class_label); 
       i = i+1;  
       class(i,1)= class_label  ;     
       class(i,2)= length(class_temp ); 
       class_label = class_label+1;   
end 
class(class(:,2)==0,:)=[];  
NoC = length(class);

Mu = zeros(NoF,NoC);  
Sigma = zeros(NoF,NoF,NoC); 
P_w = zeros(NoC,1);  
for i = class(:,1)'    
     Index_class = (Y_Train == i); 
     X_Train_classi = (X_Train(:,Index_class))'; 
    
     [Mu_temp, Sigma(:,:,i)] = Parameters(X_Train_classi);

     cov = mean(diag(Sigma(:,:,1)));
     

     Mu(:,i)= Mu_temp';
     P_w(i)=  class(i,2)/ NoTrS; 
end


g_C1 = zeros(NoC,1) ;
for Idx_test = 1:(NoTeS)    
    x = X_Test(:,Idx_test); 
    for i = class(:,1)'      
        g_C1(i)= -0.5*( (2*log(2*pi)) + (log(det(cov) + (x -Mu(:,i))'*inv(cov)*(x-Mu(:,i)))))
    end
  
   Y_Test(Idx_test) = find(g_C1 == max(g_C1)); 
end


          
