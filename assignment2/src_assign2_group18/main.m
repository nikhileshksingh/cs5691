
%% Load data {'1_linear', '1_non_linear', '1_overlapping', '2a' , '2b' }
clc;
clear; 


%[train_data, test_data, val_data, datacellTrain, datacellTest, datacellVal] = load_data('2b')
[train_data, test_data, val_data, ~, ~, ~] = load_data('1_overlapping')


X_Train = train_data(:,1:end-1)'; 
Y_Train = train_data(:,end)'; 

X_Test  = test_data(:,1:end-1)';    
Y_Test  = test_data(:,end);   
    
X_Val = val_data(:, 1:end-1)';
Y_Val = val_data(:, end);

%%
% Pred_Val = bayes_knn(train_data,X_Train',1)
% accur4 = return_accuracy_matrix(Y_Train', Pred_Val)

Pred_Val = knn_classifier(train_data,test_data,11)
accur4 = return_accuracy_matrix(Y_Test, Pred_Val')





%% Classfiers 
K =1% For gmm 
% 
% Pred_Val = C_is_sigmasq_I(X_Train, Y_Train, X_Val)
% accur1 = return_accuracy_matrix(Y_Val, Pred_Val)
% % 
% Pred_Val = C_is_same('naive',  X_Train, Y_Train, X_Val)
% accur2_naive = return_accuracy_matrix(Y_Val, Pred_Val)
% % 
% Pred_Val = C_is_same('',  X_Train, Y_Train, X_Val)
% accur2 = return_accuracy_matrix(Y_Val, Pred_Val)


% Pred_Val = C_is_different('naive', X_Train, Y_Train, X_Val)
% accur3_naive = return_accuracy_matrix(Y_Val, Pred_Val)
% % 
% Pred_Val = C_is_different('', X_Train, Y_Train, X_Train)
% accur3 = return_accuracy_matrix(Y_Train', Pred_Val)
% 
% Pred_Val = GMMClassifier('', X_Train', Y_Train', X_Train',K)
% accur4 = return_accuracy_matrix(Y_Train', Pred_Val)

% Pred_Val = GMMClassifier('naive', X_Train', Y_Train', X_Test',K)
% accur4 = return_accuracy_matrix(Y_Test, Pred_Val)


%% For 2b data . Variable length 
%noofCompo = 3
%model = GMMClassifierVarLenModel('', X_Train', Y_Train',noofCompo)

%accuracy = GMMClassifierVarLenPredict( model,X_Test,Y_Test,datacellTrain ,datacellTest, noofCompo )  





%%
% 
% xrange = -10:0.3:20
% yrange = -15:0.3:20

xrange = -2:0.03:3
yrange = -1.5:0.03:1.5

% xrange = -6:0.2:10
% yrange = -15:0.3:15

K=5

decision_surface(xrange, yrange, train_data, 'bayes_knn', 1)
%decision_surface(xrange, yrange, train_data, 'C_is_sigmasq_I', K)


