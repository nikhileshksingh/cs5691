function [train_data, test_data, val_data,datacellTrain, datacellTest, datacellVal] = load_data(i)
        datacellTrain = {}
        datacellTest ={}
        datacellVal = {}
switch i
    case '1_linear'
        relativePath = '/../data_assign2_group18/dataset1/linearly_separable/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:11
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];
        class3_test = [load(filePath(9)) ones(size(load(filePath(9)),1),1).*3];
        class3_train = [load(filePath(10)) ones(size(load(filePath(10)),1),1).*3];
        class3_val = [load(filePath(11)) ones(size(load(filePath(11)),1),1).*3];

        train_data = [class1_train ; class2_train ; class3_train]
        test_data = [class1_test ; class2_test ; class3_test]
        val_data = [class1_val ; class2_val ; class3_val]
    
    case '1_non_linear'
       
        relativePath = '/../data_assign2_group18/dataset1/nonlinearly_separable/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:8
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];

        train_data = [class1_train ; class2_train ]
        test_data = [class1_test ; class2_test ]
        val_data = [class1_val ; class2_val ]
    
    case '1_overlapping'
        relativePath = '/../data_assign2_group18/dataset1/overlapping/';
        structOfFiles = dir(strcat(pwd,relativePath))

        filePath=[]
        filePath = string(filePath)
        for i = 3:14
            filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)

        end

        class1_test = [load(filePath(3)) ones(size(load(filePath(3)),1),1)];
        class1_train = [load(filePath(4)) ones(size(load(filePath(4)),1),1)];
        class1_val = [load(filePath(5)) ones(size(load(filePath(5)),1),1)];
        class2_test = [load(filePath(6)) ones(size(load(filePath(6)),1),1).*2];
        class2_train = [load(filePath(7)) ones(size(load(filePath(7)),1),1).*2];
        class2_val = [load(filePath(8)) ones(size(load(filePath(8)),1),1).*2];
        class3_test = [load(filePath(9)) ones(size(load(filePath(6)),1),1).*3];
        class3_train = [load(filePath(10)) ones(size(load(filePath(7)),1),1).*3];
        class3_val = [load(filePath(11)) ones(size(load(filePath(8)),1),1).*3];
        class4_test = [load(filePath(12)) ones(size(load(filePath(6)),1),1).*4];
        class4_train = [load(filePath(13)) ones(size(load(filePath(7)),1),1).*4];
        class4_val = [load(filePath(14)) ones(size(load(filePath(8)),1),1).*4];

        train_data = [class1_train ; class2_train ; class3_train; class4_train ]
        test_data = [class1_test ; class2_test ; class3_test; class4_test]
        val_data = [class1_val ; class2_val ; class3_val; class4_val]
    
    case '2a'
        relativePath = '/../data_assign2_group18/dataset2/dataset_2a';
        filepath = strcat(pwd,relativePath)
        
        dataset = load(filepath)
        [train_indx val_indx test_indx] = dividerand(size(dataset,1),0.7,0.2,0.1)
        train_data = dataset(train_indx,:)
        test_data = dataset(test_indx,:)
        val_data = dataset(val_indx,:)
        

        
        
    case '2b'
        classes = ["1_bathtub" ; "2_leopards" ; "3_light-house" ; "4_mars" ; "5_airplanes"]
        train_data =[]
        test_data = []
        val_data =[]
        datacellTrain = {}
        datacellTest ={}
        datacellVal = {}
        %dataset = []
        
        for c = 1:size(classes,1) 
        relativePath = '/../data_assign2_group18/dataset2/feats_surf/train/';  % TRAIN PATH
        
        structOfFiles = dir(strcat(pwd,relativePath,classes(c),'/'))

        filePath=[]
        filePath = string(filePath)
        
         
        for i = 3: size(structOfFiles,1)
            filePath(i) = strcat(pwd,relativePath,classes(c),'/',structOfFiles(i).name)
            datacellTrain{c,i-2} = load(filePath(i))
        
        end
        tempcell = datacellTrain(c,:)
        matrix = cell2mat(tempcell')
        train_data = [train_data ; [matrix c*ones(size(matrix,1),1)]];
        %[train_indx val_indx test_indx] = dividerand(size(temp,1),0.7,0.2,0.1)
         
        %test_data = [test_data ;[temp(test_indx,:) c*ones(size(test_indx,2),1)]];
        %val_data = [val_data ;[temp(val_indx,:) c*ones(size(val_indx,2),1)]];
        end
        
        
      
        
        for c = 1:size(classes,1) 
        relativePath = '/../data_assign2_group18/dataset2/feats_surf/test/';  % TEST PATH
        
        structOfFiles = dir(strcat(pwd,relativePath,classes(c),'/'))

        filePath=[]
        filePath = string(filePath)
        
         
        for i = 3: size(structOfFiles,1)
            filePath(i) = strcat(pwd,relativePath,classes(c),'/',structOfFiles(i).name)
            datacellTest{c,i-2} = load(filePath(i))
        
        end
        tempcell = datacellTest(c,:)
        matrix = cell2mat(tempcell')
        test_data = [test_data ; [matrix c*ones(size(matrix,1),1)]];
        %[train_indx val_indx test_indx] = dividerand(size(temp,1),0.7,0.2,0.1)
         
        %test_data = [test_data ;[temp(test_indx,:) c*ones(size(test_indx,2),1)]];
        %val_data = [val_data ;[temp(val_indx,:) c*ones(size(val_indx,2),1)]];
        end
          
        
        
        
        for c = 1:size(classes,1) 
        relativePath = '/../data_assign2_group18/dataset2/feats_surf/val/';  % TRAIN PATH
        
        structOfFiles = dir(strcat(pwd,relativePath,classes(c),'/'))

        filePath=[]
        filePath = string(filePath)
        
         
        for i = 3: size(structOfFiles,1)
            filePath(i) = strcat(pwd,relativePath,classes(c),'/',structOfFiles(i).name)
            datacellVal{c,i-2} = load(filePath(i))
        
        end
        tempcell = datacellVal(c,:)
        matrix = cell2mat(tempcell')
        val_data = [val_data ; [matrix c*ones(size(matrix,1),1)]];
        %[train_indx val_indx test_indx] = dividerand(size(temp,1),0.7,0.2,0.1)
         
        %test_data = [test_data ;[temp(test_indx,:) c*ones(size(test_indx,2),1)]];
        %val_data = [val_data ;[temp(val_indx,:) c*ones(size(val_indx,2),1)]];
        end
              
        
        
%         end
%         [train_indx val_indx test_indx] = dividerand(size(dataset,1),0.7,0.2,0.1)
%         train_data = dataset(train_indx,:)
%         test_data = dataset(test_indx,:)
%         val_data = dataset(val_indx,:)
%        
%         for c = 1:size(classes,1)
%             temp = []
%             for i = 1:size(datacell,2)
%                 temp = [temp ; datacell{c,i}]
%                 
%             end 
%             totalDataCell{c} = 
%             
%         end    
        
        
        
        
    otherwise
        disp("No such dataset")




end
