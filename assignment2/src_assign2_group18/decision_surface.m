function decision_surface(xrange, yrange, train_data, classifier, K)
   
    [x , y] = meshgrid(xrange,yrange);
    mydata = [x(:) y(:)]
    X_Train = train_data(:,1:2)'; 
    Y_Train = train_data(:,3)'; 
    
    switch (classifier)
        case 'C_is_sigmasq_I' 
            prediction = C_is_sigmasq_I(X_Train, Y_Train, mydata')
        case 'C_is_same_naive' 
            prediction = C_is_same('naive',X_Train, Y_Train, mydata')    
        case 'C_is_same' 
            prediction = C_is_same('', X_Train, Y_Train, mydata')
        case 'C_is_different_naive' 
            prediction = C_is_different('naive', X_Train, Y_Train, mydata')
        case 'C_is_different' 
            prediction = C_is_different('', X_Train, Y_Train, mydata') 
        case 'gmm_naive'
            prediction = GMMClassifier('naive', X_Train', Y_Train', mydata, K)
        case 'gmm'
            prediction = GMMClassifier('', X_Train', Y_Train', mydata, K)    
        case 'bayes_knn'
            prediction = bayes_knn(train_data ,mydata, K)
            
     end
    

    mydata = [mydata prediction]
    figure;
    hold on
    for j = 1 : size(mydata,1)
        if(mydata(j,end) ==1)
            plot(mydata(j,1), mydata(j,2),'.r')
        else if (mydata(j,end) == 2)
            plot(mydata(j,1), mydata(j,2),'.b')
        else if (mydata(j,end) == 3)
            plot(mydata(j,1), mydata(j,2),'.g')        
        else 
            plot(mydata(j,1), mydata(j,2),'.y')
            end
            end
        end
    end
    plot(train_data(:,1), train_data(:,2),'*k')
    %plot(test_data(:,1), test_data(:,2),'+')
    %plot(val_data(:,1), val_data(:,2),'O')
    hold off

end