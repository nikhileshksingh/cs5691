% Function when covariance metrices are same
% 2_b and 3a in 1st part
function [Y_Test] = C_is_same(classifier,X_Train, Y_Train, X_Test)

[~  ,NoTrS] = size(X_Train); 
[NoF,NoTeS] = size(X_Test);
Y_Test=zeros(NoTeS,1);

class_label = min(Y_Train); 
i = 0; 
class = zeros(max(Y_Train),2);
for j= 1: max(Y_Train)
       class_temp = find( Y_Train == class_label); 	 
       i = i+1; 									
       class(i,1)= class_label  ;     				
       class(i,2)= length(class_temp ); 			
       class_label = class_label+1;   
end 
class(class(:,2)==0,:)=[]; 						    
NoC = length(class);

Mu = zeros(NoF,NoC);  				

P_w = zeros(NoC,1);   			
for i = class(:,1)'    			
     Index_class = (Y_Train == i); 
     X_Train_classi = (X_Train(:,Index_class))';  
	 
    
     [Mu_temp, ~] = Parameters(X_Train_classi);
   
     Mu(:,i)= Mu_temp';
     P_w(i)=  class(i,2)/ NoTrS; 
end

    [~, cov_mat] = GaussianMLEstimator(X_Train');

if (strcmp(classifier,'naive'))
for i = 1:NoF
    for j = 1:NoF
        if(i~=j)
            cov_mat(i,j) = 0
        end
    end
end
end

g_C2 = zeros(NoC,1) ;
for Idx_test = 1:(NoTeS)    
    x = X_Test(:,Idx_test); 
    for i = class(:,1)'      
        w= (cov_mat ^(-1))* Mu(:,i);
        w_0 = -1/2* Mu(:,i)'*(cov_mat ^(-1))* Mu(:,i) + log(P_w(i));
        g_C2(i)= w'* x + w_0;

    end
  
   Y_Test(Idx_test) = find(g_C2 == max(g_C2));
end
          
