function pdict_label = GMMClassifier( classifier, TrainSample, TrainLabel,TestSample, K )  



[NoTeS,NoF]=size(TestSample);
pdict_label=zeros(NoTeS,1);




for i= unique(TrainLabel)'
    
    X= TrainSample((TrainLabel==i), :);
    
    
    MODEL=gmmFunction(X,K);
    
    field.para(i)=MODEL;
    
end
NoClass=i;


% non diagonal are zero for naive
if(strcmp(classifier, 'naive') )
    for i = 1:NoClass
        
        for j = 1:K
            
            for k = 1:NoF
                for l = 1:NoF
                    if(k ~= l)
                        field.para(i).Sigma(k,l,j) = 0;
                    end
                    
                end
            end
            
        end
    end
end





for Idx_test = 1:NoTeS
    x = TestSample(Idx_test,:)';  % NoF*1
    g=zeros(NoClass,K);
    
    classlabel=0;
    maxg=-inf;
    for i= 1: NoClass
        for j=1:K
            Mu   =field.para(i).Miu;
            Sigma = field.para(i).Sigma;
            P_w  = field.para(i).Pi;
            
            g(i,j)= (-0.5* (x - Mu(:,j))'*(Sigma(:,:,j)^(-1))*(x - Mu(:,j))- NoF/2*log(2*pi)-0.5*log(det(Sigma(:,:,j)))+log(P_w(j)));
            
            if g(i,j)>maxg
                maxg=g(i,j);
                classlabel=i;
            end
        end
    end
    pdict_label(Idx_test) = classlabel
end

end


% To compute gmm Model
function model = gmmFunction(X, K)



threshold = 1e-14;


maxiter = 500; %loop upto maximum this many times

[N, D] = size(X);



[~,centroids,~,DistMatrix] = kmeans(X,K);


[pMiu,pPi,pSigma] = init_params();

Lprev = -inf;


for iter = 2:maxiter
    
    Px = real(calc_prob());
    Px(isnan(Px))=0;
    Px(isnan(Px))=0;
    
    
    pGamma = Px .* repmat(pPi, N, 1);
    pGamma = pGamma ./ repmat(sum(pGamma, 2), 1, K);
    
    
    
    Nk = sum(pGamma, 1);
    Nk(isnan(Nk))=0;
    Nk(isinf(Nk))=0;
    
    
    pMiu = diag(1./Nk) * pGamma' * X;
    pPi = Nk/N;
    
    
    
    for kk = 1:K
        Xshift = X-repmat(pMiu(kk, :), N, 1);
        pSigma(:, :, kk) = (Xshift' * ...
            (diag(pGamma(:, kk)) * Xshift)) / Nk(kk);
    end
    
    
    L = sum(log(Px*pPi'));
    if L-Lprev < threshold
        break;
    end
    Lprev = L;
    
end


model = [];
model.Miu = pMiu';
model.Sigma = pSigma;
model.Pi = pPi;




    function [pMiu,pPi,pSigma] = init_params()
        pMiu = centroids;
        pPi = zeros(1, K);
        pSigma = zeros(D, D, K);
        
        
        
        [~,labels] = min(DistMatrix,[],2);
        
        for k=1:K
            Xk = X(labels == k, :);
            pPi(k) = size(Xk, 1)/N;
            pSigma(:, :, k) = cov(Xk);
        end
    end

    function Px = calc_prob()
        
        Px = zeros(N, K);
        for k = 1:K
            Xshift = X-repmat(pMiu(k, :), N, 1);
            inv_pSigma = inv(pSigma(:, :, k));
            tmp = sum((Xshift*inv_pSigma) .* Xshift, 2);
            
            
            coef  = (2*pi)^(D/2) * sqrt(det(inv_pSigma));
            Px(:, k) = (coef^(-1)) * exp(-0.5*tmp);
            
        end
    end
end

