

% Code to perform Eigen analysis on the Covarince matrices of the given images
% Folders to perform eigen analtysis : 4 5 6 8 9 10 13 14 19 21 26 27 31 35 37
% Folder to reconstruct: randomly chosen 5 folders (any image from them)

%% Setup

clear;
clc;

% reading the folders assigned to the team
folder = pwd;
relativePath = '../data_assign1_group23/64x64/';
baseFileName = 'our _folders.txt';
absoluteFileName = fullfile(folder, relativePath, baseFileName);
folderListID = fopen(absoluteFileName,'r');
folderList = fscanf(folderListID,'%d');

%setting up variables for the random selection of images for reconstruction
folders = [ 4 5 6 8 10 13 14 19 21 26 27 31 35 37]
numelements = 5;
indices = randperm(length(folders));
indices = indices(1:numelements);
recon = folders(indices);
%recon = [10 8 13 35 27];

%% Eigen Analysis and reconstruction of images   uniSum = zeros(4096,1);
   uniSum = zeros(4096,1);
   uniPixels = []
all = [1:8]
all = [all 10:40]
for fld  = all

       
relativePath = strcat('../data_assign1_group23/64x64/', num2str(fld), '/')
pixelVector =[] %storing all the image vectors side by side for easier computation

for i = 1:10
    baseFileName = strcat(num2str(i),'.pgm');
    absoluteFileName = fullfile(folder, relativePath, baseFileName);
    image = double(imread(absoluteFileName));
    imageVector = image(:);
    pixelVector = [pixelVector imageVector];
end

uniPixels = [uniPixels pixelVector];


end
uniSum = sum(uniPixels,2);
uniMean = uniSum/size(uniPixels,2);

sumCoVector = zeros(4096,4096);

for i = 1:size(uniPixels,2)
   
   A = uniPixels(:,i) - uniMean ;
   sumCoVector = sumCoVector + (A*(A'));
    
end    

covMatrix = sumCoVector/(size(uniPixels,2)-1); %Co-varince Matrix

covfefe = cov(uniPixels');
%%

for fld  = [ 4 5 6 8 10 13 14 19 21 26 27 31 35 37]
    
    relativePath = strcat('../data_assign1_group23/64x64/', num2str(fld), '/')
    pixelVector =[] %storing all the image vectors side by side for easier computation

    for i = 1:10
        baseFileName = strcat(num2str(i),'.pgm');
        absoluteFileName = fullfile(folder, relativePath, baseFileName);
        image = double(imread(absoluteFileName));
        imageVector = image(:);
        pixelVector = [pixelVector imageVector];
    end



    [V,D] = eig(covfefe);

    %plot(D)
    
    [sortD, indices] = sort((real(diag(D))),'descend');  %abs value or not???
    sortV = V(:,indices);
    %plot(sortD)
    indices;
    sortD;
    tmp = zeros(4096,1);



    im = randi([1 10],1,1) %randomly select an image from a folder to reconstruct
    for j = [1,10,20,40,80,160,320,640,4096]

        if ismember(fld,recon)

            %for im

                for i = 1:j
                tmp = tmp + (transpose(pixelVector(:,im)) * (sortV(:,i)) ) * sortV(:,i);
                end
            tmp = tmp + uniMean;
            colormap gray
            figure;
            cmpImg = imshowpair(reshape(pixelVector(:,im),64,64), reshape(tmp,64,64) , 'montage');
            saveas(cmpImg,strcat( num2str(fld), '_', num2str(im), '_' ,num2str(j),'.png'));
            %end
        end

    end

    
end

