% Univariate function approximation.
% Domain = (0,1). Generate 100 data points randomly using below function by adding Gaussian noise with 0 mean. 
% Divide the data set as 70:20:10 (train:test:validation)

% f(x) = e^(sin(2*pi*x)) + log_e(x)

%% Generating data points for given function 
clear;
clc;


rng(100);
N = 100 % number of data points
N_use =100


xval = rand(N,1)
xval = sort(xval);

yTemp =[]
for i = 1:N
    yTemp(i) = exp((sin(2*pi*xval(i)))) + log(xval(i))

end
yTemp = transpose(yTemp)
varn = 0.09 
meanVal = 0;
t = yTemp + sqrt(varn).*randn(N,1)+meanVal;

dataset_full = [xval t yTemp]
dataset = datasample(dataset_full,N_use, 'Replace',false)
dataset = sortrows(dataset,1)

t = dataset(:,2)
%% Plotting underlying function and data points
figure;
plot(dataset_full(:,1),yTemp, 'b' )
hold on
plot(dataset(:,1),t, 'r+')
title('Univariate Data')
xlabel('')
ylabel('t')
legend('f(x)','t')

hold on

xval = dataset(:, 1)
xval = sort(xval)
yTemp = dataset(:, 3)
data = [xval t]
data2 = [xval yTemp]
%data_ovr =dataset(:, 1:2)
%data2_over = [dataset(:, 1) dataset(:, 3)]
%% Dividing data into train test and validation (70:20:10)

[trainIndx , testIndx , valIndx ] = dividerand(N_use,0.7,0.2,0.1)
trainX = data(trainIndx ,1) 
trainY = data(trainIndx ,2)

testX = data(testIndx ,1)
testY = data(testIndx ,2)

validationX = data(valIndx ,1)
validationY = data(valIndx ,2)

trainXyTemp = data2(trainIndx ,1) 
trainYyTemp = data2(trainIndx ,2)

testXyTemp = data2(testIndx ,1)
testYyTemp = data2(testIndx ,2)

validationXytemp = data2(valIndx ,1)
validationYyTemp = data2(valIndx ,2)


%% Taking N =10, M= 10
M =5

%for k = 1:M
    
    cj = zeros(M+1,1)
    x=   trainX 
    tInter =   trainY
    
    % Computing the c_j vector
    for j = 0:M
        xpow = x.^j
        tmp = tInter.*xpow
        cj(j+1) = sum(tmp)
    end

    %Computing the a_ij matrix
    A = ones(M+1,M+1);

    for i = 0:M

        for j = 0:M
            xpow = x.^(i+j)
            elem = sum(xpow)
            A(i+1,j+1) = elem;
        end

    end
%end
    % Solving the sytem of linear equations to get w

    w = linsolve(A,cj)
    
    xfinal =[]
    for i =  0: M
        temp =   x.^i    
        xfinal = [xfinal temp]

    end
    
        x_all_final =[]
    for i =  0: M
        temp =   dataset_full(:,1).^i    
        x_all_final = [x_all_final temp]
    end
    
    xfinal_test = []
        for i =  0: M
        temp =   testX.^i    
        xfinal_test = [xfinal_test temp]

        end
    
    xfinal_val = []
        for i =  0: M
        temp =   validationX.^i    
        xfinal_val = [xfinal_val temp]

        end
    
    yPredict_train = xfinal*w
    yPredict_test = xfinal_test*w
    yPredict_val = xfinal_val*w
    figure;
    plot(dataset_full(:,1),dataset_full(:,3), 'b' )
    hold on
    plot(trainX,trainY, 'go')
    %title('Overfitting in univariate Data')
    xlabel('x')
    ylabel('t')
    

    hold on
    plot(trainX, yPredict_train, 'r');
    hold off
    legend('f(x)','t', 'y' ,'Location','southeast')
    erm_train = sqrt(mean((yPredict_train - trainY).^2))
    erm_test = sqrt(mean((yPredict_test - testY).^2))
    erm_val = sqrt(mean((yPredict_val - validationY).^2))
    
%% Regularization

%powe = linspace(0,1,1000)
%powe = [-15:-1]
etr = []
ete = []
%for p = powe
lambda= exp(-0.5)
I = eye(size(A))
A_reg = A + (lambda*I)
w_reg = linsolve(A_reg,cj)
y_reg_train = xfinal*w_reg
y_reg_test = xfinal_test*w_reg

y_all_train = x_all_final*w_reg

erms_train = sqrt(mean((y_reg_train - trainY).^2))
erms_test  = sqrt(mean((y_reg_test - testY).^2))

etr = [etr erms_train]
ete = [ete erms_test]

%end

% figure;
% plot(powe, etr, 'r')
% hold on
% plot(powe, ete, 'b')
% xlabel('ln \lambda')
% ylabel('E_{rms}')
% legend('Train Error','Test Error','Location','northeast')

figure;
    plot(trainX,trainY,'O')
    hold on
    plot(dataset_full(:,1),dataset_full(:,3), 'g' )
    hold on
    plot(dataset_full(:,1),y_all_train,'r')
    legend('t-values','f(x)','y(x,w) Regularized','Location','southeast')
    xlabel('x')

%title('Lambda = 0 ,M=50')

%title('Lambda = exp(-8) ,M=50')

hold off




%% Polynomial Curve Fitting
%  Generating Figure 1.4 from bishop for given N data points
degOfPoly = [ 0 1 3  5 9 20 50] % 50 is included for regularized one 
for k = 1:size(degOfPoly,2)
    M = degOfPoly(k)
    cj = zeros(M+1,1)
    x=   xval 
    tInter =   t
    
    % Computing the c_j vector
    for j = 0:M
        xpow = x.^j
        tmp = tInter.*xpow
        cj(j+1) = sum(tmp)
    end

    %Computing the a_ij matrix
    A = ones(M+1,M+1);

    for i = 0:M

        for j = 0:M
            xpow = x.^(i+j)
            elem = sum(xpow)
            A(i+1,j+1) = elem;
        end

    end

    % Solving the sytem of linear equations to get w

    w = linsolve(A,cj)
    
    xfinal =[]
    for i =  0: M
        temp =   x.^i    
        xfinal = [xfinal temp]

    end
    
    if(M==50)
    xfinalLambda = xfinal;
    end
    yPredict = xfinal*w
    
    figure()    
    plot(xval,t,'O')
    hold on
    plot(xval,yTemp, 'g' )
    plot(xval,yPredict,'r')
    xlabel('x')
    ylabel('t')
    legend('t-values','f(x)','y(x,w)','Location','southeast')
    title(strcat('M= ', num2str(degOfPoly(k))))
end  



%% For producing scatter plot and calculating erms values for train and test dataset
%Scatter plot produced for M=9 test data , M=20 training data 

ermsArray = []


degOfPoly = [ 0 1 3  5 9 20]
for k = 1:size(degOfPoly,2)
    M = degOfPoly(k)
    cj = zeros(M+1,1)
    x=  trainX
    tInter = trainY 
    % Computing the c_j vector
    for j = 0:M
        xpow = x.^j
        tmp = tInter.*xpow
        cj(j+1) = sum(tmp)
    end

    %Computing the a_ij matrix
    A = ones(M+1,M+1);

    for i = 0:M

        for j = 0:M
            xpow = x.^(i+j)
            elem = sum(xpow)
            A(i+1,j+1) = elem;
        end

    end

    % Solving the sytem of linear equations to get w

    w = linsolve(A,cj)
    
    xfinal =[]
    for i =  0: M
        temp =   x.^i    
        xfinal = [xfinal temp]

    end
    yPredict = xfinal*w
    
    % for testX
    xfinalTest =[]
    for i =  0: M
        temp = testX.^i
        xfinalTest = [xfinalTest temp]

    end
    yPredictTest = xfinalTest*w
    
     ermsTrain  = sqrt(mean((trainY - yPredict).^2))
     ermsTest = sqrt(mean((testY - yPredictTest).^2))
     tp = [ermsTrain  ermsTest]
       ermsArray = [ermsArray ; tp]
       
    % Scatter plot for M = 9 and M = 20
    if (M==5)
        figure
        hold on
        scatter(testY,yPredictTest,'+','r')
        refline
        xlabel('Target Output')
        ylabel('Model Output')
        title('Scatter Plot for Test Data (M=5)')
        hold off
    end
    if (M==5)
        figure
        hold on
        scatter(trainY,yPredict,'+','r')
        refline
        xlabel('Target Output')
        ylabel('Model Output')
        title('Scatter Plot for Training Data (M=5)')
        hold off
    end
end  
ermsArray = [degOfPoly' ermsArray ]



%% Regularization for M=50  

% % For producing regularized plot for M = 50 at lambda ={0 exp(-8)}
% % Here xfinalLambda has to be for xfinal for M=50 
% % We have saved it as 
% lambdaVec = [0 exp(-8)] 
% for i = 1:size(lambdaVec,2)
% lambda= lambdaVec(i)
% I = eye(size(A))
% A_reg = A + (lambda*I)
% w_reg = linsolve(A_reg,cj)
% 
% y_reg = xfinalLambda*w_reg
% %erms  = sqrt(mean((y_reg - testY).^2))
% figure;
% plot(xval,t,'O')
%     hold on
%     plot(xval,yTemp, 'g' )
%     plot(xval,y_reg,'r')
% legend('t-values','f(x)','y(x,w) Regularized','Location','southeast')
% if(i==1)
% title('Lambda = 0 ,M=50')
% else
% title('Lambda = exp(-8) ,M=50')
% end
% 
% hold off
% end
%% This was for finding the optimal value of lambda



% lamRange = [ -25 : 5]
% 
% ermsArr= []
% for p = 1:size(lamRange,2)
% 
% lambda= exp(lamRange(p))
% I = eye(size(A))
% A_reg = A + (lambda*I)
% w_reg = linsolve(A_reg,cj)
% 
% y_reg = xfinalTest*w_reg    
% erms  = sqrt(mean((y_reg - testY).^2))
% ermsArr = [ermsArr erms]    
% end 
% ermsArr = [ lamRange'  ermsArr']








