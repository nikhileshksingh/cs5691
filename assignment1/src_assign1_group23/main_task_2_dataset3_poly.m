%Bivariate data <<x1,x2>,y>


%% Loading the data into variables

clear;
clc;

% reading the folders assigned to the team

relativePath = '/../data_assign1_group23/team23/bivariate_group23/bivariateData/';
structOfFiles = dir(strcat(pwd,relativePath))

filePath=[]
filePath = string(filePath)
for i = 3:8
    filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)
end

test = load(filePath(3));
train = load(filePath(4));
train100 = load(filePath(5));
train1000 = load(filePath(6));
train20 = load(filePath(7));
val = load(filePath(8));

%% Spllitting the X an Y parts of the data for ease in handling
%Just change this to change the training set
X_poly = train
 

x_train = X_poly(:,1:2);
y_train = X_poly(:,3);

x_val = val(:,1:2);
y_val = val(:, 3);

x_test = test(:, 1:2);
y_test = test(:,3);

%% Linear Model for regression using Polynomial Basis functions
%ErmsArray =[]
%ttr = [ 0 5 19]
%for i = ttr

M = 2   %degree of the polynomial
N = size(x_train,1)


[u,v] = meshgrid(0:M);

uv = [u(:),v(:)];
uv(sum(uv,2)>M,:) = []

row_totals = sum(uv,2);

[sorted, row_ids] = sort(row_totals, 'ascend');

uv = ([uv(row_ids,:)])


% Training 
p=[];
for i = 1:N
    tmp = x_train(i,2).^uv(:,1).*x_train(i,1).^uv(:,2);
    p = [p tmp ] ;
end


PHI = p';

%w = pinv(PHI)*y_train
w = (inv(PHI'*PHI))*(PHI'*y_train)



y_results_train = PHI*w

% Plotting  
% figure;
% plot3(x_train(:,1),x_train(:,2),y_train, '.')
% hold on
% plot3(x_train(:,1),x_train(:,2),y_results_train, 'x')
% xlabel('x1')
% ylabel('x2')
% zlabel('y')
% legend('Target output','Fitted Output')
% grid on
% title(strcat('M=',num2str(M)))
% %title('Target output')
% hold off



Erms_train = sqrt(mean((y_train - y_results_train).^2))
% figure 
% hold on
%     scatter(y_train,y_results_train,'+','b')
%     refline
%     xlabel('Target Output')
%     ylabel('Model Output')
%     title('Scatter Plot for train data')
%  hold off

%Validation

N_val = size(x_val,1)
v = [];
for i = 1:N_val
    tmp = x_val(i,2).^uv(:,1).*x_val(i,1).^uv(:,2);
    v = [v tmp ] ;
end
PHI_val = v';
y_results_val = PHI_val*w;
% % figure;
% % plot3(x_val(:,1),x_val(:,2),y_val, '*')
% % hold on
% % plot3(x_val(:,1),x_val(:,2),y_results_val, 'o')
% % hold off
% % 




% % Testing
% 
N = size(x_test,1);
q = [];
for i = 1:N
    tmp = x_test(i,2).^uv(:,1).*x_test(i,1).^uv(:,2);
    q = [q tmp ] ;
end
PHI_test = q';
y_results_test = PHI_test*w;


% Plot for test data plot
% figure;
% plot3(x_test(:,1),x_test(:,2),y_test, '*')
% hold on
% title('Test data plot')
% plot3(x_test(:,1),x_test(:,2),y_results_test, 'o')
% hold off

Erms_test = sqrt(mean((y_test - y_results_test).^2))
Erms_val = sqrt(mean((y_val - y_results_val).^2))
% figure 
% hold on
%     scatter(y_test,y_results_test,'+','r')
%     refline
%     xlabel('Target Output')
%     ylabel('Model Output')
%     title('Scatter Plot for Test Data')
%  hold off

%ErmsArray = [ErmsArray ;[Erms_train  Erms_test]]
%end

%ErmsArray = [ttr' ErmsArray]
%%
[minErmsTrain ,indxMinErmsTrain] = min(ErmsArray(:,2))
[minErmsTest ,indxMinErmsTest] = min(ErmsArray(:,3))
% Plot for erms value comaprison
% % figure
% % 
% % 
% % plot(ErmsArray(3:16,1),ErmsArray(3:16,2),'r')
% % hold on
% % plot(ErmsArray(3:16,1),ErmsArray(3:16,3),'g')
% % xlabel('Degree of Polynomial(M)')
% % ylabel('ERMS value')
% % legend('Train data','Test Data')
% % title('ERMS value comparision for N=20')
% % hold off
% 
% 

%% Regularization


%%%% Variables for quadratic regularization %%%
 erms_Re_Arr =[]
 %%for rr = 
lambda = exp(9);
I = eye(size(PHI'*PHI))
w_reg = (inv((PHI'*PHI) + (lambda*I)))* (PHI'*y_train) 
%%%% Regularization steps %%%

y_results_test_reg = PHI_test*w_reg;
Erms_test_reg = sqrt(mean((y_test - y_results_test_reg).^2))

y_results_train_reg = PHI*w_reg;
Erms_train_reg = sqrt(mean((y_train - y_results_train_reg).^2))

erms_Re_Arr = [erms_Re_Arr ;[Erms_train_reg  Erms_test_reg]]
 
 %end

