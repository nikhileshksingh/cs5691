%Bivariate data <<x1,x2>,y>


%% Loading the data into variables

clear;
clc;

% reading the folders assigned to the team

relativePath = '/../data_assign1_group23/team23/bivariate_group23/bivariateData/';
structOfFiles = dir(strcat(pwd,relativePath))

filePath=[]
filePath = string(filePath)
for i = 3:8
    filePath(i) = strcat(pwd,relativePath,structOfFiles(i).name)
end

test = load(filePath(3));
train = load(filePath(4));
train100 = load(filePath(5));
train1000 = load(filePath(6));
train20 = load(filePath(7));
val = load(filePath(8));

%% Spllitting the X an Y parts of the data for ease in handling
%Just change this to change the training set
X_poly = train20
 

x_train = X_poly(:,1:2);
y_train = X_poly(:,3);

x_val = val(:,1:2);
y_val = val(:, 3);

x_test = test(:, 1:2);
y_test = test(:,3);

%% Linear Model for regression using Polynomial Basis functions
ErmsArray =[]
ttr = [15]
for i = ttr

M = i %degree of the polynomial
N = size(x_train,1)


[u,v] = meshgrid(0:M);

uv = [u(:),v(:)];
uv(sum(uv,2)>M,:) = []

row_totals = sum(uv,2);

[sorted, row_ids] = sort(row_totals, 'ascend');

uv = ([uv(row_ids,:)])


%Training 
p=[];
for i = 1:N
    tmp = x_train(i,2).^uv(:,1).*x_train(i,1).^uv(:,2);
    p = [p tmp ] ;
end


PHI = p';

w = pinv(PHI)*y_train
% w = (inv(PHI'*PHI))*(PHI'*y_train)



y_results_train = PHI*w

%Plotting  
% figure;
% plot3(x_train(:,1),x_train(:,2),y_train, '.')
% hold on
% plot3(x_train(:,1),x_train(:,2),y_results_train, 'x')
% xlabel('x1')
% ylabel('x2')
% zlabel('y')
% legend('Target output','Fitted Output')
% grid on
% title(strcat('M=',num2str(M)))
% title('Target output')
% hold off
% 


Erms_train = sqrt(mean((y_train - y_results_train).^2))
% figure 
% hold on
%     scatter(y_train,y_results_train,'+','b')
%     refline
%     xlabel('Target Output')
%     ylabel('Model Output')
%     title('Scatter Plot for train data')
%  hold off

%Validation

N_val = size(x_val,1)
v = [];
for i = 1:N_val
    tmp = x_val(i,2).^uv(:,1).*x_val(i,1).^uv(:,2);
    v = [v tmp ] ;
end
PHI_val = v';
y_results_val = PHI_val*w;
% figure;
% plot3(x_val(:,1),x_val(:,2),y_val, '*')
% hold on
% plot3(x_val(:,1),x_val(:,2),y_results_val, 'o')
% hold off

Erms_val = sqrt(mean((y_val - y_results_val).^2))



%Testing

N = size(x_test,1);
q = [];
for i = 1:N
    tmp = x_test(i,2).^uv(:,1).*x_test(i,1).^uv(:,2);
    q = [q tmp ] ;
end
PHI_test = q';
y_results_test = PHI_test*w;


%Plot for test data plot
% figure;
% plot3(x_test(:,1),x_test(:,2),y_test, '*')
% hold on
% title('Test data plot')
% plot3(x_test(:,1),x_test(:,2),y_results_test, 'o')
% hold off

Erms_test = sqrt(mean((y_test - y_results_test).^2))

% figure 
% hold on
%     scatter(y_test,y_results_test,'+','r')
%     refline
%     xlabel('Target Output')
%     ylabel('Model Output')
%     title('Scatter Plot for Test Data')
%  hold off

ErmsArray = [ErmsArray ;[Erms_train  Erms_test]]
end

ErmsArray = [ttr' ErmsArray]

[minErmsTrain ,indxMinErmsTrain] = min(ErmsArray(:,2))
[minErmsTest ,indxMinErmsTest] = min(ErmsArray(:,3))
%Plot for erms value comaprison
% figure


% plot(ErmsArray(3:16,1),ErmsArray(3:16,2),'r')
% hold on
% plot(ErmsArray(3:16,1),ErmsArray(3:16,3),'g')
% xlabel('Degree of Polynomial(M)')
% ylabel('ERMS value')
% legend('Train data','Test Data')
% title('ERMS value comparision for N=20')
% hold off

figure('rend','painters','pos',[10 10 800 200])
subplot(1,3,1)
scatter(y_train, y_results_train)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(y_test, y_results_test)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(y_val, y_results_val)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')


%% 
% %% Regularization
% 

%%%% Variables for quadratic regularization %%%
 erms_Re_Arr =[]
 %%for rr = 
lambda = 0.5;
I = eye(size(PHI'*PHI))
w_reg = (inv((PHI'*PHI) + (lambda*I)))* (PHI'*y_train) 
%%%% Regularization steps %%%

y_results_val_reg = PHI_val*w_reg;
Erms_val_reg = sqrt(mean((y_val - y_results_val_reg).^2))

y_results_test_reg = PHI_test*w_reg;
Erms_test_reg = sqrt(mean((y_test - y_results_test_reg).^2))

y_results_train_reg = PHI*w_reg;
Erms_train_reg = sqrt(mean((y_train - y_results_train_reg).^2))

erms_Re_Arr = [erms_Re_Arr ;[Erms_train_reg  Erms_test_reg Erms_val_reg]]
 
 %end

% 








%% Linear Model for regression using Gaussian Basis functions

data = X_poly % Change in above section to change data set here  
rmsearravalue = []
values = [15]
for tt = values
k = tt
[PHI, variance , kmeans] = returnPhi(data,k)
[PHI_test] = returnPHIPHI(x_test , kmeans , variance ,k)
[PHI_val] = returnPHIPHI(x_val, kmeans, variance, k)

w1 = pinv(PHI)*y_train(:,1)

y1_results = PHI*w1
y1_results_test = PHI_test*w1
y1_results_val = PHI_val*w1
erms1_train = sqrt(mean((y_train(:,1) - y1_results).^2))
erms1_test = sqrt(mean((y_test(:,1) - y1_results_test).^2))
erms1_val= sqrt(mean((y_val(:,1) - y1_results_val).^2))
disp([erms1_train erms1_test])
% 
% figure;
% 
% plot3(x(:,1),x(:,2),y, '.')
% hold on
% plot3(x(:,1),x(:,2),y1_results, 'o')
% grid on
% hold off

rmsearravalue   = [rmsearravalue ;[erms1_val erms1_train  erms1_test]]
end
figure('rend','painters','pos',[10 10 800 200])
subplot(1,3,1)
scatter(y_train, y1_results)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(y_test, y1_results_test)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(y_val, y1_results_val)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')


rmsearravalue = [values' rmsearravalue]


%% Regularization for gausian basis function 

%%%% Variables for quadratic regularization %%%


erms_Re_Arr =[]
lamValVec = [0.000001]  % Change here for range of values of lambda
 for rr = lamValVec 
lambda = rr;
I = eye(size(PHI'*PHI))
w_reg = (inv((PHI'*PHI) + (lambda*I)))* (PHI'*y_train) 
%%%% Regularization steps %%%


y_results_test_reg = PHI_test*w_reg;
Erms_test_reg = sqrt(mean((y_test - y_results_test_reg).^2))

y_results_train_reg = PHI*w_reg;
Erms_train_reg = sqrt(mean((y_train - y_results_train_reg).^2))

y_results_val_reg = PHI_val*w_reg;
Erms_val_reg = sqrt(mean((y_val - y_results_val_reg).^2))

 erms_Re_Arr = [erms_Re_Arr ;[Erms_train_reg  Erms_test_reg]]
 end
erms_Re_Arr = [lamValVec' erms_Re_Arr]

figure('rend','painters','pos',[10 10 800 200])
subplot(1,3,1)
scatter(y_train, y_results_train_reg)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(y_test, y_results_test_reg)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(y_val, y_results_val_reg)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')


%% Measure of smoothness of hyper surface 
% 
lambda = 0


PHI_Tikhonov = returnPhiTilde(kmeans, variance)
phi = PHI(:,2:k+1)
phi_test = PHI_test(:,2:k+1)
phi_val = PHI_val(:,2:k+1)

w1_tik = (inv((phi'*phi) + (lambda*PHI_Tikhonov)))*(phi'*y_train)

y1_train_tik = phi*w1_tik;

erms1_train_tik = sqrt(mean((y_train - y1_train_tik).^2))


y1_test_tik = phi_test*w1_tik;

erms1_test_tik = sqrt(mean((y_test - y1_test_tik).^2))


disp ([erms1_train_tik erms1_test_tik ] )

tikhonov_reg_term1 = 0.5 *( w1_tik'*PHI_Tikhonov*w1_tik)

y1_val_tik = phi_val*w1_tik;

erms1_val_tik = sqrt(mean((y_val- y1_val_tik).^2))

figure('rend','painters','pos',[10 10 800 200])
subplot(1,3,1)
scatter(y_train, y1_train_tik)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(y_test, y1_test_tik)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(y_val, y_results_val_reg)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')


%% funtion to return Phi_tilde

function phi_tilde = returnPhiTilde(kM, var)
    k = size(kM,1)
    
    for i = 1:k
       for j = 1:k
          MuIminusMuJ = (kM(i,:) - kM(j,:))
          sqNorm = (vecnorm(MuIminusMuJ,2,2)).^2
          phi_tilde(i,j)  = exp(-sqNorm/(2*var))
       end
        
        
    end

end



%% Function for calculating PHI using K means clusttering
function [PHI ] = returnPHIPHI(data , clustMean , variance ,kval)
kMeans = clustMean
k= kval
x = data(:,1:2)
PHI = []
for i = 1:k
   
        meanRepeat = repmat(kMeans(i,:),size(x,1),1)
        dist = meanRepeat - x
        sqEucledianNorm = vecnorm(dist,2,2).^2
        colPhiElement  =     exp(-sqEucledianNorm/(2*variance))
        PHI =[PHI colPhiElement]   
end
PHI = [ones(size(x,1),1) PHI]

end




function [PHI,variance, kM] = returnPhi (somedata, kval) 
x_data = somedata(:,1:2)
   [idx C] = kmeans(x_data, kval)
mappedData = [somedata idx]
for i = 1 : kval
 temp = somedata(find(mappedData(:,end)==i),1:end)
 covTemp = cov(temp)
 varVect(i) = trace(covTemp)*(1/size(covTemp,1))
end
variance = mean(varVect)
kM =C


%     rng(973919784)
%     kMeanIndices = randi([1 size(x_data,1)],1,k)
%     kMeans = x_data(kMeanIndices,:)
% 
%     newKMeans =  zeros(k,size(x_data,2))
%     clusterIndex = zeros(size(x_data,1),1)
%     tempMat = kMeans;
%     
%     cnt=0
%     
%     while (diff(tempMat, newKMeans))
% 
%     %while (cnt<10)
%         for i = 1:size(x_data,1)
%             xRepeat = repmat(x_data(i,:),k,1)
%             dist = xRepeat - kMeans
%             sqEucledianNorm = vecnorm(dist,2,2).^2
%             [minimum,index] = min(sqEucledianNorm)
%             clusterIndex(i) = index
%         end
%     
%         for j = 1 : k
%            clusterJ = find ( clusterIndex == j)
%            xtemp = x_data(clusterJ,:)
%            newKMeans(j,:) = [sum(xtemp,1)/size(xtemp,1)]
% 
%         end
%         tempMat = kMeans
%         kMeans = newKMeans;
%         cnt= cnt+1
%     end
% 
%     mappedData = [somedata clusterIndex]
%     [val , idx ]= sort(mappedData(:,end))
%     mappedData =  mappedData(idx,:)
%     varVect = [k]
%     for i = 1 : k
%         temp = mappedData(find(mappedData(:,10)==i),1:8)
%         covTemp = cov(temp)
%         varVect(i) = trace(covTemp)*(1/size(covTemp,1))
%     end
% 
%     variance = mean(varVect)
   
   PHI = []
   for i = 1:kval
  
       meanRepeat = repmat(kM(i,:),size(x_data,1),1)
       dist = meanRepeat - x_data
       sqEucledianNorm = vecnorm(dist,2,2).^2
       colPhiElement  =     exp(-sqEucledianNorm/(2*variance))
       PHI =[PHI colPhiElement]   
   end
   PHI = [ones(size(x_data,1),1) PHI]
   kM =kM


end

function bool = diff(mat1 , mat2)
    if mat1 == mat2 
        bool = false
    else 
        bool = true
    end
end













