clear;
clc;

% reading the folders assigned to the team

relativePath = '/../data_assign1_group23/team23/Energy_Efficiency_Dataset/energy-data.txt';
filePath = strcat(pwd,relativePath)

%% Divinding train, validate and test data

data = load(filePath)
N = size(data,1)

[trainIndx , testIndx , valIndx ] = dividerand(N,0.07,0.2,0.1)
x_train = data(trainIndx ,1:8) 
t_train = data(trainIndx ,9:10)
t1_train = t_train(:,1)
t2_train = t_train(:,2)
data_train1 = [x_train t1_train]
data_train2 = [x_train t2_train]


x_test = data(testIndx ,1:8) 
t_test = data(testIndx ,9:10)
t1_test = t_test(:,1)
t2_test = t_test(:,2)
data_test1 = [x_test t1_test]
data_test2 = [x_test t2_test]

x_val = data(valIndx ,1:8) 
t_val = data(valIndx ,9:10)
t1_val = t_val(:,1)
t2_val = t_val(:,2)
data_val1 = [x_val t1_val]
data_val2 = [x_val t2_val]


%% Getting the PHI 
k=20 % this is equivalent to M. 

k1 = k
k2 = k

[PHI1,var_train1, kM_train1] = returnPhi(data_train1,k1)
[PHI_test1] =  returnPHIPHI(data_test1 , kM_train1 , var_train1 ,k1)
[PHI_val1] =   returnPHIPHI(data_val1 , kM_train1 , var_train1 ,k1)

[PHI2,var_train2, kM_train2] = returnPhi(data_train2,k2)
[PHI_test2] =  returnPHIPHI(data_test2 , kM_train2 , var_train2 ,k2)
[PHI_val2] =  returnPHIPHI(data_val2 , kM_train2 , var_train2 ,k2)
%% Getting Model from PHI8
w1 = (inv(PHI1'*PHI1))* (PHI1'*t1_train)
%w1 = pinv(PHI)*t1_train

y1_train = PHI1*w1

w2 = (inv(PHI2'*PHI2))* (PHI2'*t2_train)
%w2 = pinv(PHI)*t2_train


y2_train = PHI2*w2

erms1_train = sqrt(mean((t1_train - y1_train).^2))
erms2_train = sqrt(mean((t2_train - y2_train).^2))


y1_test = PHI_test1*w1;
y2_test = PHI_test2*w2;
erms1_test = sqrt(mean((t1_test - y1_test).^2));
erms2_test = sqrt(mean((t2_test - y2_test).^2));


y1_val = PHI_val1*w1
y2_val = PHI_val2*w2
erms1_val = sqrt(mean((t1_val - y1_val).^2))
erms2_val = sqrt(mean((t2_val - y2_val).^2))

disp ([erms1_train erms1_test erms1_val erms2_train erms2_test erms2_val] )
%%

figure('rend', 'painters', 'pos', [10,10,800,200])
subplot(1,3,1)
scatter(t1_train, y1_train)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(t1_test, y1_test)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(t1_val, y1_val)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')

figure('rend', 'painters', 'pos', [10,10,800,200])
subplot(1,3,1)
scatter(t2_train, y2_train)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(t2_test, y2_test)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(t2_val, y2_val)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')

% t_val = data(valIndx ,9:10)
% t1_val = t_val(:,1)
% t2_val = t_val(:,2)
% data_val1 = [x_val t1_val]
% data_val2 = [x_val t2_val]




%% Adding Regularization

lambda = 0.00001
I1 = eye(size(PHI1'*PHI1))
I2 = eye(size(PHI2'*PHI2))
w1_reg = (inv((PHI1'*PHI1) + (lambda*I1)))* (PHI1'*t1_train) 
w2_reg = (inv((PHI2'*PHI2) + (lambda*I2)))* (PHI2'*t2_train) 

%%%% Regularization step
t_val = data(valIndx ,9:10)
t1_val = t_val(:,1)
t2_val = t_val(:,2)
data_val1 = [x_val t1_val]
data_val2 = [x_val t2_val]
 %%%

y1_train_reg = PHI1*w1_reg;
y2_train_reg = PHI2*w2_reg;
erms1_train_reg = sqrt(mean((t1_train - y1_train_reg).^2))
erms2_train_reg = sqrt(mean((t2_train - y2_train_reg).^2))

y1_val_reg = PHI_val1*w1_reg;
y2_val_reg = PHI_val2*w2_reg;
erms1_val_reg = sqrt(mean((t1_val - y1_val_reg).^2))
erms2_val_reg = sqrt(mean((t2_val - y2_val_reg).^2))

y1_test_reg = PHI_test1*w1_reg;
y2_test_reg = PHI_test2*w2_reg;
erms1_test_reg = sqrt(mean((t1_test - y1_test_reg).^2))
erms2_test_reg = sqrt(mean((t2_test - y2_test_reg).^2))

disp ([erms1_train_reg erms1_val_reg erms1_test_reg erms2_train_reg erms2_val_reg erms2_test_reg] )

figure('rend', 'painters', 'pos', [10,10,800,200])
subplot(1,3,1)
scatter(t1_train, y1_train_reg)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(t1_test, y1_test_reg)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(t1_val, y1_val_reg)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')

figure('rend', 'painters', 'pos', [10,10,800,200])
subplot(1,3,1)
scatter(t2_train, y2_train_reg)
xlabel('Target')
ylabel('Prediction')
title('Train Data')
subplot(1,3,2)
scatter(t2_test, y2_test_reg)
xlabel('Target')
ylabel('Prediction')
title('Test Data')
subplot(1,3,3)
scatter(t2_val, y2_val_reg)
xlabel('Target')
ylabel('Prediction')
title('Validation Data')

%scatter(y2_test_reg, t2_test)
%% Comparing the erms with and without regularization
disp(['Without reg '  '  with reg'])
disp([ erms1_train  erms1_train_reg])
disp([ erms1_test  erms1_test_reg])
disp([ erms1_val  erms1_val_reg])

disp(['Without reg '  '  with reg'])
disp([ erms2_train  erms2_train_reg])
disp([ erms2_test  erms2_test_reg])
disp([ erms2_val  erms2_val_reg])

%% Measure of smoothness of hyper-surface
% lambda = 0
% 
% 
% PHI_Tikhonov1 = returnPhiTilde(kM_train1, var_train1)
% phi1 = PHI1(:,2:k1+1)
% phi_test1 = PHI_test1(:,2:k1+1)
% 
% PHI_Tikhonov2 = returnPhiTilde(kM_train2, var_train2)
% phi2 = PHI2(:,2:k2+1)
% phi_test2 = PHI_test2(:,2:k2+1)
% 
% 
% w1_tik = (inv((phi1'*phi1) + (lambda*PHI_Tikhonov1)))*(phi1'*t1_train)
% w2_tik = (inv((phi2'*phi2) + (lambda*PHI_Tikhonov2)))*(phi2'*t2_train)
% 
% y1_train_tik = phi1*w1_tik;
% y2_train_tik = phi2*w2_tik;
% erms1_train_tik = sqrt(mean((t1_train - y1_train_tik).^2))
% erms2_train_tik = sqrt(mean((t2_train - y2_train_tik).^2))
% 
% y1_test_tik = phi_test1*w1_tik;
% y2_test_tik = phi_test2*w2_tik;
% erms1_test_tik = sqrt(mean((t1_test - y1_test_tik).^2))
% erms2_test_tik = sqrt(mean((t2_test - y2_test_tik).^2))
% 
% disp ([erms1_train_tik erms1_test_tik erms2_train_tik erms2_test_tik] )
% 
% tikhonov_reg_term1 = 0.5 *( w1_tik'*PHI_Tikhonov1*w1_tik)
% tikhonov_reg_term2 = 0.5 *( w2_tik'*PHI_Tikhonov2*w2_tik)

%% funtion to return Phi_tilde

function phi_tilde = returnPhiTilde(kM, var)
    k = size(kM,1)
    
    for i = 1:k
       for j = 1:k
          MuIminusMuJ = (kM(i,:) - kM(j,:))
          sqNorm = (vecnorm(MuIminusMuJ,2,2)).^2
          phi_tilde(i,j)  = exp(-sqNorm/(2*var))
       end
        
        
    end
 
end



%% function to return PHI

function [PHI ] = returnPHIPHI(data , clustMean , variance ,kval)
kMeans = clustMean
k= kval
x = data(:,1:8)
PHI = []
for i = 1:k
   
        meanRepeat = repmat(kMeans(i,:),size(x,1),1)
        dist = meanRepeat - x
        sqEucledianNorm = vecnorm(dist,2,2).^2
        colPhiElement  =     exp(-sqEucledianNorm/(2*variance))
        PHI =[PHI colPhiElement]   
end
PHI = [ones(size(x,1),1) PHI]
end






function [PHI,variance, kM] = returnPhi (somedata, k) % this is equivalent to M. 
    
x_data = somedata(:,1:8)
    [idx C] = kmeans(x_data, k)
mappedData = [somedata idx]
for i = 1 : k
  temp = somedata(find(mappedData(:,end)==i),1:end)
  covTemp = cov(temp)
  varVect(i) = trace(covTemp)*(1/size(covTemp,1))
end
variance = mean(varVect)
kM =C
 
 
%     rng(973919784)
%     kMeanIndices = randi([1 size(x_data,1)],1,k)
%     kMeans = x_data(kMeanIndices,:)
% 
%     newKMeans =  zeros(k,size(x_data,2))
%     clusterIndex = zeros(size(x_data,1),1)
%     tempMat = kMeans;
%     
%     cnt=0
%     
%     while (diff(tempMat, newKMeans))
% 
%     %while (cnt<10)
%         for i = 1:size(x_data,1)
%             xRepeat = repmat(x_data(i,:),k,1)
%             dist = xRepeat - kMeans
%             sqEucledianNorm = vecnorm(dist,2,2).^2
%             [minimum,index] = min(sqEucledianNorm)
%             clusterIndex(i) = index
%         end
%     
%         for j = 1 : k
%            clusterJ = find ( clusterIndex == j)
%            xtemp = x_data(clusterJ,:)
%            newKMeans(j,:) = [sum(xtemp,1)/size(xtemp,1)]
% 
%         end
%         tempMat = kMeans
%         kMeans = newKMeans;
%         cnt= cnt+1
%     end
% 
%     mappedData = [somedata clusterIndex]
%     [val , idx ]= sort(mappedData(:,end))
%     mappedData =  mappedData(idx,:)
%     varVect = [k]
%     for i = 1 : k
%         temp = mappedData(find(mappedData(:,10)==i),1:8)
%         covTemp = cov(temp)
%         varVect(i) = trace(covTemp)*(1/size(covTemp,1))
%     end
% 
%     variance = mean(varVect)
    
    PHI = []
    for i = 1:k
   
        meanRepeat = repmat(kM(i,:),size(x_data,1),1)
        dist = meanRepeat - x_data
        sqEucledianNorm = vecnorm(dist,2,2).^2
        colPhiElement  =     exp(-sqEucledianNorm/(2*variance))
        PHI =[PHI colPhiElement]   
    end
    PHI = [ones(size(x_data,1),1) PHI]
    kM =kM
end

%% function to tell if two matrices are same

function bool = diff(mat1 , mat2)
    if mat1 == mat2 
        bool = false
    else 
        bool = true
    end
end